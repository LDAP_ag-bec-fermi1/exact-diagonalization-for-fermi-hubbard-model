#import matplotlib.pyplot as plt
import numpy as np
from scipy.linalg import expm, sinm, cosm
import time as tm
from scipy.special import comb
import random as rnd
import itertools
import sys
from scipy.linalg import eig
#import ED_func
#from numpy.linalg import multi_dot
#for python 3

def array_order2(a, b, k):
	#k=len(np.array(a))
	out_put=0
	if list(a)>list(b):
		out_put=1
	if list(b)>list(a):
		out_put=-1
	return out_put
	#return np.sign(np.sum((np.array(a)-np.array(b))*100**np.arange(k, 0, -1)))

	

def indx2(c, V):
	imin=0
	k=len(np.array(c))
	imax=len(V)-1
	iavg=int((imin+imax)/2)
	x=array_order2(c, V[iavg], k)
	if array_order2(c, V[imin], k)==0:
		x=0
		iavg=imin
	if array_order2(c, V[imax], k)==0:
		x=0
		iavg=imax
	while x!=0:
		imin=imin*(1-x)/2+iavg*(1+x)/2
		imax=imax*(1+x)/2+iavg*(1-x)/2
		iavg=int((imin+imax)/2)
		x=array_order2(c, V[iavg], k)
	return iavg
	
	
def Projection(State, Center, l, p):
	ellipticity=np.linspace(1, 1, len(State))
	State1=ellipticity*np.abs(np.sort(State)-np.sort(Center))
	if p=='infty':
		return int(max(State1)<l)
	else:
		p=float(p)
		return int(sum(State1**p)<l**p)
		
		
def my_mat_exp(U1, N, V, d ):
	U_up1=np.zeros((d, d), dtype=complex)
	for i in range(d):
		v=V[i]
		for j in range(d):
			u=V[j]
			D=np.zeros((N, N), dtype=complex)
			for atm1 in range(N):
				for atm2 in range(N):
					D[atm1, atm2]=U1[u[atm1], v[atm2]]
			U_up1[i, j]=np.linalg.det(D)
	return U_up1
	
	
def convergence_reduce(X, indices):
    k1=np.shape(X)[0]-1
    #print k1
    seq=np.array([0,1, 2,3, 4])
    X=(indices**(seq[k1])).transpose()*(X.transpose([1,0,2]))
    #X=X.transpose([1,0,2])
    matrix=[[indices[0][i]**(seq[k1-j]) for j in range(k1+1)] for i in range(k1+1)]
    matrix=np.linalg.inv(matrix)
    return np.dot(matrix, X)[0]
	

	
def Hamiltonian(L,kup,kdn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U):
	#m=6*loc_len+1
	#k=loc_len
	dup=int(comb(L, kup))
	ddn=int(comb(L, kdn))
	Hop_up=np.zeros((dup, dup))
	Hop_dn=np.zeros((ddn, ddn))
	V_up=[]
	V_dn=[]
	NO_up=[]
	NO_dn=[]
	#State_Out=np.zeros((dup, ddn))
	for subset in itertools.combinations(np.arange(L),kup):
		V_up.append(subset)
		NO_up.append(sum(np.array(subset)%2))
	for subset in itertools.combinations(np.arange(L),kdn):
		V_dn.append(subset)
		NO_dn.append(sum(np.array(subset)%2))
	A_up=np.eye(kup)
	A_dn=np.eye(kdn)
	H12=np.zeros((dup, ddn))
	if kup!=0:
		for config in range(dup):
			#print config, dup
			v=np.array(V_up[config])
			#print v
			for atm in range(kup-1):
				#H12[config][v[atm]]=1
				if np.abs(v[atm]-v[atm+1])!=1:
					#print atm, config
					config2=indx2(v+A_up[atm], V_up)
					
					#print atm, config, config2
					#State_Out[config, :]=State_Out[config, :]+J*State_In[config2, :]
					#State_Out[config2, :]=State_Out[config2, :]+J*State_In[config, :]
					Hop_up[config][config2]=1
					Hop_up[config2][config]=1
			if v[kup-1]<L-1:
				#print v, A[k-1]
				config2=indx2(v+A_up[kup-1], V_up)
				#State_Out[config, :]=State_Out[config, :]+J*State_In[config2, :]
				#State_Out[config2, :]=State_Out[config2, :]+J*State_In[config, :]
				Hop_up[config][config2]=1
				Hop_up[config2][config]=1
				#print config
			elif v[0]!=0: # periodic bc
				config2=indx2(np.sort((v+A_up[kup-1])%L), V_up)
				#State_Out[config, :]=State_Out[config, :]+J*State_In[config2, :]
				#State_Out[config2, :]=State_Out[config2, :]+J*State_In[config, :]
				#Hop_up[config][config2]=1
				#Hop_up[config2][config]=1
			#H12[config][v[kup-1]]=1
	if kdn!=0:
		for config in range(ddn):
			#print config, d
			v=np.array(V_dn[config])
			#print v
			for atm in range(kdn-1):
				#H12[config][v[atm]]=1
				if np.abs(v[atm]-v[atm+1])!=1:
					config2=indx2(v+A_dn[atm], V_dn)
					#State_Out[:, config]=State_Out[ :, config]+J*State_In[ :, config2]
					#State_Out[:, config2]=State_Out[ :, config2]+J*State_In[ :, config]
					Hop_dn[config][config2]=1
					Hop_dn[config2][config]=1
			if v[kdn-1]<L-1:
				#print v, A[k-1]
				config2=indx2(v+A_dn[kdn-1], V_dn)
				#State_Out[:, config]=State_Out[ :, config]+J*State_In[ :, config2]
				#State_Out[:, config2]=State_Out[ :, config2]+J*State_In[ :, config]
				Hop_dn[config][config2]=1
				Hop_dn[config2][config]=1
				#print config
			elif v[0]!=0:
				config2=indx2(np.sort((v+A_dn[kdn-1])%L), V_dn)
				#State_Out[:, config]=State_Out[ :, config]+J*State_In[ :, config2]
				#State_Out[:, config2]=State_Out[ :, config2]+J*State_In[ :, config]
				Hop_dn[config][config2]=1
				Hop_dn[config2][config]=1
			#H12[config][v[kdn-1]]=1
	if kup!=0 and kdn!=0:
		for config1 in range(dup):
			for config2 in range(ddn):
				H12[config1][config2]=len(set(V_up[config1])&set(V_dn[config2]))
	V_rr= DeltaRR*np.random.random(L)
	beta=532.0/738.0
	#print beta
	def onsitepot(j):
		return V_rr[j]+DeltaAA*np.cos(beta*2*np.pi*j+phiAA) +DeltaS1*(j-L/2)+alpha*(j-L/2)**2
	def onsitepot2(j):
		return V_rr[j]+DeltaAA*np.cos(beta*2*np.pi*j+phiAA) +DeltaS2*(j-L/2)+alpha*(j-L/2)**2
	Hdiag_up=np.zeros(dup)
	Hdiag_dn=np.zeros(ddn)
	if kup!=0:
		for l in range(dup):
			Hdiag_up[l]=np.sum(onsitepot(np.array(V_up[l])))
	if kdn!=0:
		for l in range(ddn):
			Hdiag_dn[l]=np.sum(onsitepot2(np.array(V_dn[l])))
	H_up=J*Hop_up+np.diagflat(Hdiag_up)
	H_dn=J*Hop_dn+np.diagflat(Hdiag_dn)
	#Hint=U*np.diagflat(np.reshape(H12,(1, dup*ddn) ))
	#H12=U*H12
	#H=np.kron(H_up, np.eye(ddn))+np.kron(np.eye(dup), H_dn)+Hint
	
	
	return H_up, H_dn, H12, V_up, V_dn, NO_up, NO_dn
	
	
def Hamiltonian_trunc(L,kup,kdn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, up_index, dn_index):
	#m=6*loc_len+1
	#k=loc_len
	trunc_dim=len(up_index)
	dup=int(comb(L, kup))
	ddn=int(comb(L, kdn))
	Hop_up=np.zeros((dup, dup))
	Hop_dn=np.zeros((ddn, ddn))
	V_up=[]
	V_dn=[]
	NO_up=[]
	NO_dn=[]
	#State_Out=np.zeros((dup, ddn))
	for subset in itertools.combinations(np.arange(L),kup):
		V_up.append(subset)
		NO_up.append(sum(np.array(subset)%2))
	for subset in itertools.combinations(np.arange(L),kdn):
		V_dn.append(subset)
		NO_dn.append(sum(np.array(subset)%2))
	A_up=np.eye(kup)
	A_dn=np.eye(kdn)
	H12=np.zeros((dup, ddn))
	if kup!=0:
		for config in range(dup):
			#print config, dup
			v=np.array(V_up[config])
			#print v
			for atm in range(kup-1):
				#H12[config][v[atm]]=1
				if np.abs(v[atm]-v[atm+1])!=1:
					#print atm, config
					config2=indx2(v+A_up[atm], V_up)
					
					#print atm, config, config2
					#State_Out[config, :]=State_Out[config, :]+J*State_In[config2, :]
					#State_Out[config2, :]=State_Out[config2, :]+J*State_In[config, :]
					Hop_up[config][config2]=1
					Hop_up[config2][config]=1
			if v[kup-1]<L-1:
				#print v, A[k-1]
				config2=indx2(v+A_up[kup-1], V_up)
				#State_Out[config, :]=State_Out[config, :]+J*State_In[config2, :]
				#State_Out[config2, :]=State_Out[config2, :]+J*State_In[config, :]
				Hop_up[config][config2]=1
				Hop_up[config2][config]=1
				#print config
			elif v[0]!=0:
				config2=indx2(np.sort((v+A_up[kup-1])%L), V_up)
				#State_Out[config, :]=State_Out[config, :]+J*State_In[config2, :]
				#State_Out[config2, :]=State_Out[config2, :]+J*State_In[config, :]
				Hop_up[config][config2]=1
				Hop_up[config2][config]=1
			#H12[config][v[kup-1]]=1
	if kdn!=0:
		for config in range(ddn):
			#print config, d
			v=np.array(V_dn[config])
			#print v
			for atm in range(kdn-1):
				#H12[config][v[atm]]=1
				if np.abs(v[atm]-v[atm+1])!=1:
					config2=indx2(v+A_dn[atm], V_dn)
					#State_Out[:, config]=State_Out[ :, config]+J*State_In[ :, config2]
					#State_Out[:, config2]=State_Out[ :, config2]+J*State_In[ :, config]
					Hop_dn[config][config2]=1
					Hop_dn[config2][config]=1
			if v[kdn-1]<L-1:
				#print v, A[k-1]
				config2=indx2(v+A_dn[kdn-1], V_dn)
				#State_Out[:, config]=State_Out[ :, config]+J*State_In[ :, config2]
				#State_Out[:, config2]=State_Out[ :, config2]+J*State_In[ :, config]
				Hop_dn[config][config2]=1
				Hop_dn[config2][config]=1
				#print config
			elif v[0]!=0:
				config2=indx2(np.sort((v+A_dn[kdn-1])%L), V_dn)
				#State_Out[:, config]=State_Out[ :, config]+J*State_In[ :, config2]
				#State_Out[:, config2]=State_Out[ :, config2]+J*State_In[ :, config]
				Hop_dn[config][config2]=1
				Hop_dn[config2][config]=1
			#H12[config][v[kdn-1]]=1
	if kup!=0 and kdn!=0:
		for config1 in range(dup):
			for config2 in range(ddn):
				H12[config1][config2]=len(set(V_up[config1])&set(V_dn[config2]))
	V_rr= DeltaRR*np.random.random(L)
	beta=532.0/738.0
	#print beta
	def onsitepot(j):
		return V_rr[j]+DeltaAA*np.cos(beta*2*np.pi*j+phiAA) +DeltaS1*(j-L/2)+alpha*(j-L/2)**2
	def onsitepot2(j):
		return V_rr[j]+DeltaAA*np.cos(beta*2*np.pi*j+phiAA) +DeltaS2*(j-L/2)+alpha*(j-L/2)**2
	Hdiag_up=np.zeros(dup)
	Hdiag_dn=np.zeros(ddn)
	if kup!=0:
		for l in range(dup):
			Hdiag_up[l]=np.sum(onsitepot(np.array(V_up[l])))
	if kdn!=0:
		for l in range(ddn):
			Hdiag_dn[l]=np.sum(onsitepot2(np.array(V_dn[l])))
	H=np.zeros((trunc_dim, trunc_dim))
	up_index_set=list(set(list(up_index)))
	dn_index_set=list(set(list(dn_index)))
	for i in range(trunc_dim):
		int_term=U*len(set(V_up[up_index[i]])&set(V_dn[dn_index[i]]))
		H[i, i]=H[i, i]+Hdiag_up[up_index[i]]+Hdiag_dn[dn_index[i]]+int_term
		vec=np.zeros(dup)
		vec[up_index[i]]=1
		vec=np.dot(Hop_up, vec)
		index_set=filter(lambda x: vec[x]>0, np.arange(dup))
		for a in index_set:
			lst1=filter(lambda x: up_index_set[x]==a, np.arange(len(up_index_set)))
			if len(lst1)>0:
				lst2=filter(lambda x: dn_index[x]==dn_index[i] and up_index[x]==a, np.arange(len(dn_index)) )
				if len(lst2)>1:
					print ('FAIL')
				if len(lst2)==1:
					H[i, lst2[0]]=J
		vec=np.zeros(ddn)
		vec[dn_index[i]]=1
		vec=np.dot(Hop_dn, vec)
		index_set=filter(lambda x: vec[x]>0., np.arange(ddn))
		for a in index_set:
			lst1=filter(lambda x: dn_index_set[x]==a, np.arange(len(dn_index_set)))
			if len(lst1)>0:
				lst2=filter(lambda x: up_index[x]==up_index[i] and dn_index[x]==a, np.arange(len(dn_index)) )
				if len(lst2)>1:
					print ('FAIL')
				if len(lst2)==1:
					H[i, lst2[0]]=J
		
	del(Hop_up)
	del(Hop_dn)
	return H
	
	
def Hamiltonian_trunc_k(L,kup,kdn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, up_index, dn_index):
	#m=6*loc_len+1
	#k=loc_len
	trunc_dim=len(up_index)
	dup=int(comb(L, kup))
	ddn=int(comb(L, kdn))
	Hop_up=np.zeros((dup, dup))
	Hop_dn=np.zeros((ddn, ddn))
	V_up=[]
	V_dn=[]
	NO_up=[]
	NO_dn=[]
	#State_Out=np.zeros((dup, ddn))
	for subset in itertools.combinations(np.arange(L),kup):
		V_up.append(subset)
		NO_up.append(sum(np.array(subset)%2))
	for subset in itertools.combinations(np.arange(L),kdn):
		V_dn.append(subset)
		NO_dn.append(sum(np.array(subset)%2))
	A_up=np.eye(kup)
	A_dn=np.eye(kdn)
	H12=np.zeros((dup, ddn))
	if kup!=0:
		for config in range(dup):
			#print config, dup
			v=np.array(V_up[config])
			#print v
			for atm in range(kup-1):
				#H12[config][v[atm]]=1
				if np.abs(v[atm]-v[atm+1])!=1:
					#print atm, config
					config2=indx2(v+A_up[atm], V_up)
					
					#print atm, config, config2
					#State_Out[config, :]=State_Out[config, :]+J*State_In[config2, :]
					#State_Out[config2, :]=State_Out[config2, :]+J*State_In[config, :]
					Hop_up[config][config2]=1
					Hop_up[config2][config]=1
			if v[kup-1]<L-1:
				#print v, A[k-1]
				config2=indx2(v+A_up[kup-1], V_up)
				#State_Out[config, :]=State_Out[config, :]+J*State_In[config2, :]
				#State_Out[config2, :]=State_Out[config2, :]+J*State_In[config, :]
				Hop_up[config][config2]=1
				Hop_up[config2][config]=1
				#print config
			elif v[0]!=0:
				config2=indx2(np.sort((v+A_up[kup-1])%L), V_up)
				#State_Out[config, :]=State_Out[config, :]+J*State_In[config2, :]
				#State_Out[config2, :]=State_Out[config2, :]+J*State_In[config, :]
				Hop_up[config][config2]=1
				Hop_up[config2][config]=1
			#H12[config][v[kup-1]]=1
	if kdn!=0:
		for config in range(ddn):
			#print config, d
			v=np.array(V_dn[config])
			#print v
			for atm in range(kdn-1):
				#H12[config][v[atm]]=1
				if np.abs(v[atm]-v[atm+1])!=1:
					config2=indx2(v+A_dn[atm], V_dn)
					#State_Out[:, config]=State_Out[ :, config]+J*State_In[ :, config2]
					#State_Out[:, config2]=State_Out[ :, config2]+J*State_In[ :, config]
					Hop_dn[config][config2]=1
					Hop_dn[config2][config]=1
			if v[kdn-1]<L-1:
				#print v, A[k-1]
				config2=indx2(v+A_dn[kdn-1], V_dn)
				#State_Out[:, config]=State_Out[ :, config]+J*State_In[ :, config2]
				#State_Out[:, config2]=State_Out[ :, config2]+J*State_In[ :, config]
				Hop_dn[config][config2]=1
				Hop_dn[config2][config]=1
				#print config
			elif v[0]!=0:
				config2=indx2(np.sort((v+A_dn[kdn-1])%L), V_dn)
				#State_Out[:, config]=State_Out[ :, config]+J*State_In[ :, config2]
				#State_Out[:, config2]=State_Out[ :, config2]+J*State_In[ :, config]
				Hop_dn[config][config2]=1
				Hop_dn[config2][config]=1
			#H12[config][v[kdn-1]]=1
	if kup!=0 and kdn!=0:
		for config1 in range(dup):
			for config2 in range(ddn):
				H12[config1][config2]=len(set(V_up[config1])&set(V_dn[config2]))
	V_rr= DeltaRR*np.random.random(L)
	beta=532.0/738.0
	#print beta
	def onsitepot(j):
		return V_rr[j]+DeltaAA*np.cos(beta*2*np.pi*j+phiAA) +DeltaS1*(j-L/2)+alpha*(j-L/2)**2
	def onsitepot2(j):
		return V_rr[j]+DeltaAA*np.cos(beta*2*np.pi*j+phiAA) +DeltaS2*(j-L/2)+alpha*(j-L/2)**2
	Hdiag_up=np.zeros(dup)
	Hdiag_dn=np.zeros(ddn)
	if kup!=0:
		for l in range(dup):
			Hdiag_up[l]=np.sum(onsitepot(np.array(V_up[l])))
	if kdn!=0:
		for l in range(ddn):
			Hdiag_dn[l]=np.sum(onsitepot2(np.array(V_dn[l])))
	Hd=np.zeros((trunc_dim, trunc_dim))
	up_index_set=list(set(list(up_index)))
	dn_index_set=list(set(list(dn_index)))
	for i in range(trunc_dim):
		int_term=U*len(set(V_up[up_index[i]])&set(V_dn[dn_index[i]]))
		Hd[i, i]=Hd[i, i]+(Hdiag_up[up_index[i]]+Hdiag_dn[dn_index[i]]+int_term)
	H1=	np.zeros((trunc_dim, trunc_dim))
	H2=np.zeros((trunc_dim, trunc_dim))
	H1sq=np.zeros((trunc_dim, trunc_dim))
	H2sq=np.zeros((trunc_dim, trunc_dim))
	for i in range(trunc_dim):	
		vec=np.zeros(dup)
		vec[up_index[i]]=1
		vec=np.dot(Hop_up, vec)
		index_set=filter(lambda x: vec[x]>0, np.arange(dup))
		for a in index_set:
			lst1=filter(lambda x: up_index_set[x]==a, np.arange(len(up_index_set)))
			if len(lst1)>0:
				lst2=filter(lambda x: dn_index[x]==dn_index[i] and up_index[x]==a, np.arange(len(dn_index)) )
				if len(lst2)>1:
					print ('FAIL')
				if len(lst2)==1:
					H1[i, lst2[0]]=1
		vec=np.dot(Hop_up, vec)
		index_set=filter(lambda x: vec[x]>0, np.arange(dup))
		for a in index_set:
			lst1=filter(lambda x: up_index_set[x]==a, np.arange(len(up_index_set)))
			if len(lst1)>0:
				lst2=filter(lambda x: dn_index[x]==dn_index[i] and up_index[x]==a, np.arange(len(dn_index)) )
				if len(lst2)>1:
					print ('FAIL')
				if len(lst2)==1:
					H1sq[i, lst2[0]]=vec[a]
		vec=np.zeros(ddn)
		vec[dn_index[i]]=1
		vec=np.dot(Hop_dn, vec)
		index_set=filter(lambda x: vec[x]>0., np.arange(ddn))
		for a in index_set:
			lst1=filter(lambda x: dn_index_set[x]==a, np.arange(len(dn_index_set)))
			if len(lst1)>0:
				lst2=filter(lambda x: up_index[x]==up_index[i] and dn_index[x]==a, np.arange(len(dn_index)) )
				if len(lst2)>1:
					print ('FAIL')
				if len(lst2)==1:
					H2[i, lst2[0]]=1
		vec=np.dot(Hop_dn, vec)
		index_set=filter(lambda x: vec[x]>0., np.arange(ddn))
		for a in index_set:
			lst1=filter(lambda x: dn_index_set[x]==a, np.arange(len(dn_index_set)))
			if len(lst1)>0:
				lst2=filter(lambda x: up_index[x]==up_index[i] and dn_index[x]==a, np.arange(len(dn_index)) )
				if len(lst2)>1:
					print ('FAIL')
				if len(lst2)==1:
					H2sq[i, lst2[0]]=vec[a]
	#H1=np.dot(Hd, H1)+np.dot(H1, Hd)
	#H2=np.dot(Hd, H2)+np.dot(H2, Hd)
	H12=np.zeros((trunc_dim, trunc_dim))
	for i in range(trunc_dim):
		vec_up=np.zeros(dup)
		vec_up[up_index[i]]=1
		vec_up=np.dot(Hop_up, vec_up)
		vec_dn=np.zeros(ddn)
		vec_dn[dn_index[i]]=1
		vec_dn=np.dot(Hop_dn, vec_dn)
		for i_up in range(dup):
			if vec_up[i_up]>0:
				for i_dn in range(ddn):
					if vec_dn[i_dn]>0:
						lst=filter(lambda x: up_index[x]==i_up and dn_index[x]==i_dn, np.arange(trunc_dim))
						if len(lst)==1:
							H12[i, lst[0]]=1+H12[i, lst[0]]
	del(Hop_up)
	del(Hop_dn)
	H=Hd+J*H1+J*H2 
	H1ac=np.dot(Hd, H1)+np.dot(H1, Hd)
	H2ac=np.dot(Hd, H2)+np.dot(H2, Hd)
	#print H1sq
	#print np.dot(H1, H1)
	H1sq=np.dot(H1, H1)
	H2sq=np.dot(H2, H2)
	H12o=np.dot(H1, H2)+np.dot(H2, H1)
	print( H12)
	print( H12o)
	#return np.dot(H, H)#np.dot(Hd, Hd)+J*H1ac+J*H2ac+2*J**2*H12+J**2*H1sq+J**2*H2sq
	return np.dot(Hd, Hd)+J*H1ac+J*H2ac+J**2*H12+J**2*H1sq+J**2*H2sq
	
	
def Hamiltonian_trunc_k_test(L,kup,kdn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, up_index, dn_index, i_cut, ord):
	#m=6*loc_len+1
	#k=loc_len
	trunc_dim=len(up_index)
	dup=int(comb(L, kup))
	ddn=int(comb(L, kdn))
	Hop_up=np.zeros((dup, dup))
	Hop_dn=np.zeros((ddn, ddn))
	V_up=[]
	V_dn=[]
	NO_up=[]
	NO_dn=[]
	#State_Out=np.zeros((dup, ddn))
	for subset in itertools.combinations(np.arange(L),kup):
		V_up.append(subset)
		NO_up.append(sum(np.array(subset)%2))
	for subset in itertools.combinations(np.arange(L),kdn):
		V_dn.append(subset)
		NO_dn.append(sum(np.array(subset)%2))
	A_up=np.eye(kup)
	A_dn=np.eye(kdn)
	H12=np.zeros((dup, ddn))
	if kup!=0:
		for config in range(dup):
			#print config, dup
			v=np.array(V_up[config])
			#print v
			for atm in range(kup-1):
				#H12[config][v[atm]]=1
				if np.abs(v[atm]-v[atm+1])!=1:
					#print atm, config
					config2=indx2(v+A_up[atm], V_up)
					
					#print atm, config, config2
					#State_Out[config, :]=State_Out[config, :]+J*State_In[config2, :]
					#State_Out[config2, :]=State_Out[config2, :]+J*State_In[config, :]
					Hop_up[config][config2]=1
					Hop_up[config2][config]=1
			if v[kup-1]<L-1:
				#print v, A[k-1]
				config2=indx2(v+A_up[kup-1], V_up)
				#State_Out[config, :]=State_Out[config, :]+J*State_In[config2, :]
				#State_Out[config2, :]=State_Out[config2, :]+J*State_In[config, :]
				Hop_up[config][config2]=1
				Hop_up[config2][config]=1
				#print config
			elif v[0]!=0:
				config2=indx2(np.sort((v+A_up[kup-1])%L), V_up)
				#State_Out[config, :]=State_Out[config, :]+J*State_In[config2, :]
				#State_Out[config2, :]=State_Out[config2, :]+J*State_In[config, :]
				Hop_up[config][config2]=1
				Hop_up[config2][config]=1
			#H12[config][v[kup-1]]=1
	if kdn!=0:
		for config in range(ddn):
			#print config, d
			v=np.array(V_dn[config])
			#print v
			for atm in range(kdn-1):
				#H12[config][v[atm]]=1
				if np.abs(v[atm]-v[atm+1])!=1:
					config2=indx2(v+A_dn[atm], V_dn)
					#State_Out[:, config]=State_Out[ :, config]+J*State_In[ :, config2]
					#State_Out[:, config2]=State_Out[ :, config2]+J*State_In[ :, config]
					Hop_dn[config][config2]=1
					Hop_dn[config2][config]=1
			if v[kdn-1]<L-1:
				#print v, A[k-1]
				config2=indx2(v+A_dn[kdn-1], V_dn)
				#State_Out[:, config]=State_Out[ :, config]+J*State_In[ :, config2]
				#State_Out[:, config2]=State_Out[ :, config2]+J*State_In[ :, config]
				Hop_dn[config][config2]=1
				Hop_dn[config2][config]=1
				#print config
			elif v[0]!=0:
				config2=indx2(np.sort((v+A_dn[kdn-1])%L), V_dn)
				#State_Out[:, config]=State_Out[ :, config]+J*State_In[ :, config2]
				#State_Out[:, config2]=State_Out[ :, config2]+J*State_In[ :, config]
				Hop_dn[config][config2]=1
				Hop_dn[config2][config]=1
			#H12[config][v[kdn-1]]=1
	if kup!=0 and kdn!=0:
		for config1 in range(dup):
			for config2 in range(ddn):
				H12[config1][config2]=len(set(V_up[config1])&set(V_dn[config2]))
	V_rr= DeltaRR*np.random.random(L)
	beta=532.0/738.0
	#print beta
	def onsitepot(j):
		return V_rr[j]+DeltaAA*np.cos(beta*2*np.pi*j+phiAA) +DeltaS1*(j-L/2)+alpha*(j-L/2)**2
	def onsitepot2(j):
		return V_rr[j]+DeltaAA*np.cos(beta*2*np.pi*j+phiAA) +DeltaS2*(j-L/2)+alpha*(j-L/2)**2
	Hdiag_up=np.zeros(dup)
	Hdiag_dn=np.zeros(ddn)
	if kup!=0:
		for l in range(dup):
			Hdiag_up[l]=np.sum(onsitepot(np.array(V_up[l])))
	if kdn!=0:
		for l in range(ddn):
			Hdiag_dn[l]=np.sum(onsitepot2(np.array(V_dn[l])))
	Hd=np.zeros((trunc_dim, trunc_dim))
	up_index_set=list(set(list(up_index)))
	dn_index_set=list(set(list(dn_index)))
	for i in range(trunc_dim):
		int_term=U*len(set(V_up[up_index[i]])&set(V_dn[dn_index[i]]))
		Hd[i, i]=Hd[i, i]+(Hdiag_up[up_index[i]]+Hdiag_dn[dn_index[i]]+int_term)
	H1=	np.zeros((trunc_dim, trunc_dim))
	H2=np.zeros((trunc_dim, trunc_dim))
	H1sq=np.zeros((trunc_dim, trunc_dim))
	H2sq=np.zeros((trunc_dim, trunc_dim))
	for i in range(trunc_dim):	
		vec=np.zeros(dup)
		vec[up_index[i]]=1
		vec=np.dot(Hop_up, vec)
		index_set=filter(lambda x: vec[x]>0, np.arange(dup))
		for a in index_set:
			lst1=filter(lambda x: up_index_set[x]==a, np.arange(len(up_index_set)))
			if len(lst1)>0:
				lst2=filter(lambda x: dn_index[x]==dn_index[i] and up_index[x]==a, np.arange(len(dn_index)) )
				if len(lst2)>1:
					print ('FAIL')
				if len(lst2)==1:
					H1[i, lst2[0]]=1
		vec=np.dot(Hop_up, vec)
		index_set=filter(lambda x: vec[x]>0, np.arange(dup))
		#for a in index_set:
		#	lst1=filter(lambda x: up_index_set[x]==a, np.arange(len(up_index_set)))
		#	if len(lst1)>0:
		#		lst2=filter(lambda x: dn_index[x]==dn_index[i] and up_index[x]==a, np.arange(len(dn_index)) )
		#		if len(lst2)>1:
		#			print 'FAIL'
		#		if len(lst2)==1:
		#			H1sq[i, lst2[0]]=vec[a]
		vec=np.zeros(ddn)
		vec[dn_index[i]]=1
		vec=np.dot(Hop_dn, vec)
		index_set=filter(lambda x: vec[x]>0., np.arange(ddn))
		for a in index_set:
			lst1=filter(lambda x: dn_index_set[x]==a, np.arange(len(dn_index_set)))
			if len(lst1)>0:
				lst2=filter(lambda x: up_index[x]==up_index[i] and dn_index[x]==a, np.arange(len(dn_index)) )
				if len(lst2)>1:
					print ('FAIL')
				if len(lst2)==1:
					H2[i, lst2[0]]=1
		vec=np.dot(Hop_dn, vec)
		index_set=filter(lambda x: vec[x]>0., np.arange(ddn))
		#for a in index_set:
	#		lst1=filter(lambda x: dn_index_set[x]==a, np.arange(len(dn_index_set)))
		#	if len(lst1)>0:
		#		lst2=filter(lambda x: up_index[x]==up_index[i] and dn_index[x]==a, np.arange(len(dn_index)) )
		#		if len(lst2)>1:
		#			print 'FAIL'
		#		if len(lst2)==1:
		#			H2sq[i, lst2[0]]=vec[a]
	#H1=np.dot(Hd, H1)+np.dot(H1, Hd)
	#H2=np.dot(Hd, H2)+np.dot(H2, Hd)
	#H12=np.zeros((trunc_dim, trunc_dim))
	#for i in range(trunc_dim):
	#	vec_up=np.zeros(dup)
	#	vec_up[up_index[i]]=1
	#	vec_up=np.dot(Hop_up, vec_up)
	#	vec_dn=np.zeros(ddn)
	#	vec_dn[dn_index[i]]=1
	#	vec_dn=np.dot(Hop_dn, vec_dn)
	#	for i_up in range(dup):
	#		if vec_up[i_up]>0:
	#			for i_dn in range(ddn):
	#				if vec_dn[i_dn]>0:
	#					lst=filter(lambda x: up_index[x]==i_up and dn_index[x]==i_dn, np.arange(trunc_dim))
	#					if len(lst)==1:
	#						H12[i, lst[0]]=1+H12[i, lst[0]]
	del(Hop_up)
	del(Hop_dn)
	H=Hd+J*H1+J*H2
	Hsq=H
	for ord_0 in range(ord-1):
		Hsq=np.dot(Hsq, H)
	Hsq=Hsq[:i_cut, :i_cut]
	H=H[:i_cut, :i_cut]
	Hsq_old=np.dot(H, H)
	
	#H1ac=np.dot(Hd, H1)+np.dot(H1, Hd)
	#H2ac=np.dot(Hd, H2)+np.dot(H2, Hd)
	#print H1sq
	#print np.dot(H1, H1)
	#H1sq=np.dot(H1, H1)
	#H2sq=np.dot(H2, H2)
	#H12=np.dot(H1, H2)+np.dot(H2, H1)
	#return np.dot(H, H)#np.dot(Hd, Hd)+J*H1ac+J*H2ac+2*J**2*H12+J**2*H1sq+J**2*H2sq
	#return Hsq
	return Hsq_old
	
	
def Hamiltonian_eff(L,kup,kdn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, up_index, dn_index):
	#m=6*loc_len+1
	#k=loc_len
	trunc_dim=len(up_index)
	dup=int(comb(L, kup))
	ddn=int(comb(L, kdn))
	Hop_up=np.zeros((dup, dup))
	Hop_dn=np.zeros((ddn, ddn))
	V_up=[]
	V_dn=[]
	NO_up=[]
	NO_dn=[]
	#State_Out=np.zeros((dup, ddn))
	for subset in itertools.combinations(np.arange(L),kup):
		V_up.append(subset)
		NO_up.append(sum(np.array(subset)%2))
	for subset in itertools.combinations(np.arange(L),kdn):
		V_dn.append(subset)
		NO_dn.append(sum(np.array(subset)%2))
	A_up=np.eye(kup)
	A_dn=np.eye(kdn)
	H12=np.zeros((dup, ddn))
	
	V_rr= DeltaRR*np.random.random(L)
	beta=532.0/738.0
	#print beta
	def onsitepot(j):
		return V_rr[j]+DeltaAA*np.cos(beta*2*np.pi*j+phiAA) +DeltaS1*(j-L/2)+alpha*(j-L/2)**2
	def onsitepot2(j):
		return V_rr[j]+DeltaAA*np.cos(beta*2*np.pi*j+phiAA) +DeltaS2*(j-L/2)+alpha*(j-L/2)**2
	Hdiag_up=np.zeros(dup)
	Hdiag_dn=np.zeros(ddn)
	if kup!=0:
		for l in range(dup):
			Hdiag_up[l]=np.sum(onsitepot(np.array(V_up[l])))
	if kdn!=0:
		for l in range(ddn):
			Hdiag_dn[l]=np.sum(onsitepot2(np.array(V_dn[l])))
	H=np.zeros((trunc_dim, trunc_dim))
	up_index_set=list(set(list(up_index)))
	dn_index_set=list(set(list(dn_index)))
	for i in range(trunc_dim):
		int_term=U*(1-J**2/DeltaS1**2)*len(set(V_up[up_index[i]])&set(V_dn[dn_index[i]]))
		H[i, i]=int_term
		vec=np.zeros(dup)
		vec[up_index[i]]=1
		vec=np.dot(Hop_up, vec)
		index_set=filter(lambda x: vec[x]>0, np.arange(dup))
		for a in index_set:
			lst1=filter(lambda x: up_index_set[x]==a, np.arange(len(up_index_set)))
			if len(lst1)>0:
				lst2=filter(lambda x: dn_index[x]==dn_index[i] and up_index[x]==a, np.arange(len(dn_index)) )
				if len(lst2)>1:
					print ('FAIL')
				if len(lst2)==1:
					H[i, lst2[0]]=J
		vec=np.zeros(ddn)
		vec[dn_index[i]]=1
		vec=np.dot(Hop_dn, vec)
		index_set=filter(lambda x: vec[x]>0., np.arange(ddn))
		for a in index_set:
			lst1=filter(lambda x: dn_index_set[x]==a, np.arange(len(dn_index_set)))
			if len(lst1)>0:
				lst2=filter(lambda x: up_index[x]==up_index[i] and dn_index[x]==a, np.arange(len(dn_index)) )
				if len(lst2)>1:
					print ('FAIL')
				if len(lst2)==1:
					H[i, lst2[0]]=J
		
	del(Hop_up)
	del(Hop_dn)
	return H
	
	
def Hamiltonian_new(Lup, Ldn, kup,kdn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, State_up, State_dn, sw):
	#m=6*loc_len+1
	#k=loc_len
	tb=tm.time()
	L=max(Lup, Ldn)
	##Basis Construction
	offset=L/2+1-min(min(State_up), min(State_dn))
	State_up=offset+State_up
	State_dn=offset+State_dn
	L2=L/2+1+max(max(State_up), max(State_dn))
	V_up=[]
	V_dn=[]
	NO_up=[]
	NO_dn=[]
	#State_Out=np.zeros((dup, ddn))
	#print State_up, State_dn
	for subset in itertools.combinations(np.arange(L2),kup):
		if Projection(np.array(subset), State_up, Lup/2, sw)==1:
			V_up.append(subset)
			NO_up.append(sum(np.array(subset)%2))
	for subset in itertools.combinations(np.arange(State_dn[0]-Ldn/2, L2),kdn):
		if subset[0]>State_dn[0]+Ldn/2:
			break
		#if subset[0]==State_dn[0]+Ldn/2 and subset[1]>State_dn[1]+Ldn/2:
		#	break
		#if subset[0]==State_dn[0]+Ldn/2 and subset[1]==State_dn[1]+Ldn/2 and subset[2]>State_dn[2]+Ldn/2:
		#		break
		if Projection(np.array(subset), State_dn, Ldn/2, sw)==1:
			V_dn.append(subset)
			NO_dn.append(sum(np.array(subset)%2))
	dup=len(NO_up)	
	ddn=len(NO_dn)
	print( 'basis done', tm.time()-tb)
	#print dup, ddn, L2
	Hop_up=np.zeros((dup, dup))
	Hop_dn=np.zeros((ddn, ddn))
	A_up=np.eye(kup)
	A_dn=np.eye(kdn)
	H12=np.zeros((dup, ddn))
	if kup!=0:
		for config in range(dup):
			#print config, dup
			v=np.array(V_up[config])
			#print v, config
			for atm in range(kup-1):
				#H12[config][v[atm]]=1
				if np.abs(v[atm]-v[atm+1])!=1:
					if Projection(np.sort(v+A_up[atm]), State_up, Lup/2, sw)==1:
						config2=indx2(np.sort(v+A_up[atm]), V_up)
						Hop_up[config][config2]=1
						Hop_up[config2][config]=1
				#print config, '1'
			if v[kup-1]<L2-1:
				if Projection(np.sort(v+A_up[kup-1]), State_up, Lup/2, sw)==1:
					config2=indx2(v+A_up[kup-1], V_up)
					Hop_up[config][config2]=1
					Hop_up[config2][config]=1
				#print config, '2'
			elif v[0]!=0:
				if Projection(np.sort((v+A_up[kup-1])%L2), State_up, Lup/2, sw)==1:
					config2=indx2(np.sort((v+A_up[kup-1])%L2), V_up)
					Hop_up[config][config2]=1
					Hop_up[config2][config]=1
				#print config, '3'
			#H12[config][v[kup-1]]=1
	if kdn!=0:
		for config in range(ddn):
			v=np.array(V_dn[config])
			for atm in range(kdn-1):
				#H12[config][v[atm]]=1
				if np.abs(v[atm]-v[atm+1])!=1:
					if Projection(v+A_dn[atm], State_dn, Ldn/2, sw)==1:
						config2=indx2(v+A_dn[atm], V_dn)
						Hop_dn[config][config2]=1
						Hop_dn[config2][config]=1
			if v[kdn-1]<L2-1:
				if Projection(v+A_dn[kdn-1], State_dn, Ldn/2, sw)==1:
					config2=indx2(v+A_dn[kdn-1], V_dn)
					Hop_dn[config][config2]=1
					Hop_dn[config2][config]=1
				#print config
			elif v[0]!=0:
				if Projection(np.sort((v+A_dn[kdn-1])%L2), State_dn, Ldn/2, sw)==1:
					config2=indx2(np.sort((v+A_dn[kdn-1])%L2), V_dn)
					Hop_dn[config][config2]=1
					Hop_dn[config2][config]=1
			#H12[config][v[kdn-1]]=1
	if kup!=0 and kdn!=0:
		for config1 in range(dup):
			for config2 in range(ddn):
				H12[config1][config2]=len(set(V_up[config1])&set(V_dn[config2]))
	V_rr= DeltaRR*np.random.random(L2)
	beta=532.0/738.0
	#print beta
	def onsitepot(j):
		return V_rr[j]+DeltaAA*np.cos(beta*2*np.pi*j+phiAA) +DeltaS1*(j-L2/2)+alpha*(j-L2/2)**2
	def onsitepot2(j):
		return V_rr[j]+DeltaAA*np.cos(beta*2*np.pi*j+phiAA) +DeltaS2*(j-L2/2)+alpha*(j-L2/2)**2
	Hdiag_up=np.zeros(dup)
	Hdiag_dn=np.zeros(ddn)
	if kup!=0:
		for l in range(dup):
			Hdiag_up[l]=np.sum(onsitepot(np.array(V_up[l])))
	if kdn!=0:
		for l in range(ddn):
			Hdiag_dn[l]=np.sum(onsitepot2(np.array(V_dn[l])))
	H_up=J*Hop_up+np.diagflat(Hdiag_up)
	H_dn=J*Hop_dn+np.diagflat(Hdiag_dn)
	#Hint=U*np.diagflat(np.reshape(H12,(1, dup*ddn) ))
	#H12=U*H12
	#H=np.kron(H_up, np.eye(ddn))+np.kron(np.eye(dup), H_dn)+Hint
	
	
	return H_up, H_dn, H12, V_up, V_dn, NO_up, NO_dn
	
	
def Hamiltonian_new_hop_sep(Lup, Ldn, kup,kdn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, State_up, State_dn, sw):
	#m=6*loc_len+1
	#k=loc_len
	tb=tm.time()
	L=max(Lup, Ldn)
	##Basis Construction
	offset=L/2+1-min(min(State_up), min(State_dn))
	State_up=offset+State_up
	State_dn=offset+State_dn
	L2=L/2+1+max(max(State_up), max(State_dn))
	V_up=[]
	V_dn=[]
	NO_up=[]
	NO_dn=[]
	#State_Out=np.zeros((dup, ddn))
	#print State_up, State_dn
	for subset in itertools.combinations(np.arange(State_up[0]-Lup/2, L2),kup):
		if Projection(np.array(subset), State_up, Lup/2, sw)==1:
			V_up.append(subset)
			NO_up.append(sum(np.array(subset)%2))
	for subset in itertools.combinations(np.arange(State_dn[0]-Ldn/2, L2),kdn):
		if subset[0]>State_dn[0]+Ldn/2:
			break
		if subset[0]==State_dn[0]+Ldn/2 and subset[1]>State_dn[1]+Ldn/2:
			break
		#if subset[0]==State_dn[0]+Ldn/2 and subset[1]==State_dn[1]+Ldn/2 and subset[2]>State_dn[2]+Ldn/2:
		#	break
		if Projection(np.array(subset), State_dn, Ldn/2, sw)==1:
			V_dn.append(subset)
			NO_dn.append(sum(np.array(subset)%2))
	dup=len(NO_up)	
	ddn=len(NO_dn)
	print( 'basis done', tm.time()-tb)
	#print dup, ddn, L2
	Hop_up=np.zeros((dup, dup))
	Hop_dn=np.zeros((ddn, ddn))
	A_up=np.eye(kup)
	A_dn=np.eye(kdn)
	H12=np.zeros((dup, ddn))
	if kup!=0:
		for config in range(dup):
			#print config, dup
			v=np.array(V_up[config])
			#print v, config
			for atm in range(kup-1):
				#H12[config][v[atm]]=1
				if np.abs(v[atm]-v[atm+1])!=1:
					if Projection(np.sort(v+A_up[atm]), State_up, Lup/2, sw)==1:
						config2=indx2(np.sort(v+A_up[atm]), V_up)
						Hop_up[config][config2]=1
						Hop_up[config2][config]=1
				#print config, '1'
			if v[kup-1]<L2-1:
				if Projection(np.sort(v+A_up[kup-1]), State_up, Lup/2, sw)==1:
					config2=indx2(v+A_up[kup-1], V_up)
					Hop_up[config][config2]=1
					Hop_up[config2][config]=1
				#print config, '2'
			elif v[0]!=0:
				if Projection(np.sort((v+A_up[kup-1])%L2), State_up, Lup/2, sw)==1:
					config2=indx2(np.sort((v+A_up[kup-1])%L2), V_up)
					Hop_up[config][config2]=1
					Hop_up[config2][config]=1
				#print config, '3'
			#H12[config][v[kup-1]]=1
	if kdn!=0:
		for config in range(ddn):
			v=np.array(V_dn[config])
			for atm in range(kdn-1):
				#H12[config][v[atm]]=1
				if np.abs(v[atm]-v[atm+1])!=1:
					if Projection(v+A_dn[atm], State_dn, Ldn/2, sw)==1:
						config2=indx2(v+A_dn[atm], V_dn)
						Hop_dn[config][config2]=1
						Hop_dn[config2][config]=1
			if v[kdn-1]<L2-1:
				if Projection(v+A_dn[kdn-1], State_dn, Ldn/2, sw)==1:
					config2=indx2(v+A_dn[kdn-1], V_dn)
					Hop_dn[config][config2]=1
					Hop_dn[config2][config]=1
				#print config
			elif v[0]!=0:
				if Projection(np.sort((v+A_dn[kdn-1])%L2), State_dn, Ldn/2, sw)==1:
					config2=indx2(np.sort((v+A_dn[kdn-1])%L2), V_dn)
					Hop_dn[config][config2]=1
					Hop_dn[config2][config]=1
			#H12[config][v[kdn-1]]=1
	if kup!=0 and kdn!=0:
		for config1 in range(dup):
			for config2 in range(ddn):
				H12[config1][config2]=len(set(V_up[config1])&set(V_dn[config2]))
	V_rr= DeltaRR*np.random.random(L2)
	beta=532.0/738.0
	#print beta
	def onsitepot(j):
		return V_rr[j]+DeltaAA*np.cos(beta*2*np.pi*j+phiAA) +DeltaS1*(j-L2/2)+alpha*(j-L2/2)**2
	def onsitepot2(j):
		return V_rr[j]+DeltaAA*np.cos(beta*2*np.pi*j+phiAA) +DeltaS2*(j-L2/2)+alpha*(j-L2/2)**2
	Hdiag_up=np.zeros(dup)
	Hdiag_dn=np.zeros(ddn)
	if kup!=0:
		for l in range(dup):
			Hdiag_up[l]=np.sum(onsitepot(np.array(V_up[l])))
	if kdn!=0:
		for l in range(ddn):
			Hdiag_dn[l]=np.sum(onsitepot2(np.array(V_dn[l])))
	H_up=J*Hop_up#+np.diagflat(Hdiag_up)
	H_dn=J*Hop_dn#+np.diagflat(Hdiag_dn)
	#Hint=U*np.diagflat(np.reshape(H12,(1, dup*ddn) ))
	#H12=U*H12
	#H=np.kron(H_up, np.eye(ddn))+np.kron(np.eye(dup), H_dn)+Hint
	
	
	return H_up, H_dn, H12, Hdiag_up, Hdiag_dn, V_up, V_dn, NO_up, NO_dn
	
def tevol_old(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, ti, tf, n):
	dup=int(comb(L, Nup))
	ddn=int(comb(L, Ndn))
	H_up, H_dn, H12, V_up, V_dn, NO_up, NO_dn=Hamiltonian(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U)
	NE_up=Nup-np.array(NO_up)
	NE_dn=Ndn-np.array(NO_dn)
	
	#initial state
	#State=np.kron(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(np.reshape(H12, (1, dup*ddn)))))
	State=np.outer(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(H12)))
	State = State/np.sqrt(sum(sum(np.abs(State)**2)))
	#print sum(sum(np.abs(State)**2))
	#print NE_up/2
	#time evol
	T=np.linspace(ti, tf, n+1)
	dt=T[1]-T[0]
	
	dU_up=expm(-1j*H_up*dt)
	dU_dn=expm(-1j*H_dn*dt)
	dU_ud=np.exp(-1j*H12*U*dt)
	
	#obswervables
	NE_up=np.outer( NE_up, np.ones(ddn))
	NE_dn=np.outer( np.ones(dup), NE_dn)
	NEUP=[]
	NOUP=[]
	NEDN=[]
	NODN=[]
	#State=np.dot(Ui, State)
	#State2=np.transpose(State)[0]
	NEUP.append(sum(sum(NE_up*np.abs(State)**2)))
	NEDN.append(sum(sum(np.abs(State)**2*NE_dn)))
	
	for ktevol in range(n):
		State = np.dot(dU_up, State)
		State = np.dot(State, dU_dn)
		State=dU_ud*State
		NEUP.append(sum(sum(NE_up*np.abs(State)**2)))
		NEDN.append(sum(sum(np.abs(State)**2*NE_dn)))
		
	return np.array(T)/(2*np.pi), np.array(NEUP), np.array(NEDN)
 
 
def tevol(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, up_atm, dn_atm, U, ti, tf, n, r):
	t1s=tm.time()
	dup=int(comb(L, Nup))
	ddn=int(comb(L, Ndn))
	H_up, H_dn, H12, V_up, V_dn, NO_up, NO_dn=Hamiltonian(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U)
	NE_up=Nup-np.array(NO_up)
	NE_dn=Ndn-np.array(NO_dn)
	
	#initial state
	#State=np.kron(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(np.reshape(H12, (1, dup*ddn)))))
	State_up, State_dn=np.zeros(dup), np.zeros(ddn)
	State_up[indx2(up_atm, V_up)], State_dn[indx2(dn_atm, V_dn)]=1, 1
	State=np.outer(State_up, State_dn)
	#State=np.outer(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(H12)))
	State = State/np.sqrt(sum(sum(np.abs(State)**2)))
	#print sum(sum(np.abs(State)**2))
	#print NE_up/2
	#time evol
	T=np.linspace(ti, tf, n+1)
	dt=T[1]-T[0]
	#r=100
	dU_up=expm(-1j*H_up*dt/r)
	del(H_up)
	dU_dn=expm(-1j*H_dn*dt/r)
	del(H_dn)
	dU_ud=np.exp(-1j*H12*U*dt/r)
	del(H12)
	t2s=tm.time()
	print('defn+exponentiation', t2s-t1s)
	
	#obswervables
	NE_up=np.outer( NE_up, np.ones(ddn))
	NE_dn=np.outer( np.ones(dup), NE_dn)
	NEUP=[]
	NOUP=[]
	NEDN=[]
	NODN=[]
	t_buff=0
	while t_buff<ti:
		t_buff=t_buff+dt/r
		State = np.dot(dU_up, State)
		State = np.dot(State, dU_dn)
		State=dU_ud*State
	
	#State=np.dot(Ui, State)
	#State2=np.transpose(State)[0]
	NEUP.append(sum(sum(NE_up*np.abs(State)**2)))
	NEDN.append(sum(sum(np.abs(State)**2*NE_dn)))
	#dU=[dU_up, dU_dn, dU_ud]
	for ktevol in range(n):
		t2s=tm.time()
		#State=State-1j*dt*np.dot(H_up, State)-1j*dt*np.dot(State, H_dn)-1j*dt*U*H12*State
		#State = State/np.sqrt(sum(sum(np.abs(State)**2)))
		for sub_t in range(int(r)):
			State = dU_ud*np.dot(dU_up, np.dot(State, dU_dn))
			#State=dU_ud*multi_dot(dU_up, State, dU_dn)
			#State = dU_ud*np.dot(State, dU_dn)
			#State=dU_ud*State
		NEUP.append(sum(sum(NE_up*np.abs(State)**2)))
		NEDN.append(sum(sum(np.abs(State)**2*NE_dn)))
		print('time step '+str(ktevol)+' of '+str(n)+' took '+ str(tm.time()-t2s) + ' s')
	del(dU_up)
	del(dU_dn)
	del(dU_ud)
	return np.array(T)/(2*np.pi), np.array(NEUP), np.array(NEDN) 
	
def tevol_new_exp(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, up_atm, dn_atm, U, ti, tf, n, r):
	t1s=tm.time()
	dup=int(comb(L, Nup))
	ddn=int(comb(L, Ndn))
	H_up1, H_dn1, H12, V_up, V_dn, NO_up, NO_dn=Hamiltonian(L,1,1, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U)
	del(H12)
	del(V_up)
	del(V_dn)
	del(NO_up)
	del(NO_dn)
	H_up, H_dn, H12, V_up, V_dn, NO_up, NO_dn=Hamiltonian(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U)
	del(H_up)
	del(H_dn)
	NE_up=Nup-np.array(NO_up)
	NE_dn=Ndn-np.array(NO_dn)
	
	#initial state
	#State=np.kron(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(np.reshape(H12, (1, dup*ddn)))))
	State_up, State_dn=np.zeros(dup), np.zeros(ddn)
	State_up[indx2(up_atm, V_up)], State_dn[indx2(dn_atm, V_dn)]=1, 1
	State=np.outer(State_up, State_dn)
	#State=np.outer(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(H12)))
	State = State/np.sqrt(sum(sum(np.abs(State)**2)))
	#print sum(sum(np.abs(State)**2))
	#print NE_up/2
	#time evol
	T=np.linspace(ti, tf, n+1)
	dt=T[1]-T[0]
	#r=100
	U1=expm(-1j*H_up1*dt/r)
	#U2=expm(-1j*H_dn1*dt/r)
	dU_up=my_mat_exp(U1, Nup, V_up, dup )
	#dU_dn=my_mat_exp(U2, Ndn, V_dn, ddn )
	
	dU_ud=np.exp(-1j*H12*U*dt/r)
	del(H12)
	t2s=tm.time()
	print('defn+exponentiation', t2s-t1s)
	
	#obswervables
	NE_up=np.outer( NE_up, np.ones(ddn))
	NE_dn=np.outer( np.ones(dup), NE_dn)
	NEUP=[]
	NEDN=[]
	t_buff=0
	while t_buff<ti:
		t_buff=t_buff+dt/r
		State = np.dot(dU_up, State)
		State = np.dot(State, dU_up)
		State=dU_ud*State
	
	#State=np.dot(Ui, State)
	#State2=np.transpose(State)[0]
	NEUP.append(sum(sum(NE_up*np.abs(State)**2)))
	NEDN.append(sum(sum(np.abs(State)**2*NE_dn)))
	'no memory issues so far'
	#dU=[dU_up, dU_dn, dU_ud]
	for ktevol in range(n):
		t2s=tm.time()
		#State=State-1j*dt*np.dot(H_up, State)-1j*dt*np.dot(State, H_dn)-1j*dt*U*H12*State
		#State = State/np.sqrt(sum(sum(np.abs(State)**2)))
		for sub_t in range(int(r)):
			State = dU_ud*np.dot(dU_up, np.dot(State, dU_up))
			#State=dU_ud*multi_dot(dU_up, State, dU_dn)
			#State = dU_ud*np.dot(State, dU_dn)
			#State=dU_ud*State
		NEUP.append(sum(sum(NE_up*np.abs(State)**2)))
		NEDN.append(sum(sum(np.abs(State)**2*NE_dn)))
		print('time step '+str(ktevol)+' of '+str(n)+' took '+ str(tm.time()-t2s) + ' s')
	del(dU_up)
	del(dU_ud)
	return np.array(T)/(2*np.pi), np.array(NEUP), np.array(NEDN) 
	
def tevol_occupancy(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, up_atm, dn_atm, U, ti, tf, n, r):
	t1s=tm.time()
	dup=int(comb(L, Nup))
	ddn=int(comb(L, Ndn))
	H_up, H_dn, H12, V_up, V_dn, NO_up, NO_dn=Hamiltonian(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U)
	NE_up=Nup-np.array(NO_up)
	NE_dn=Ndn-np.array(NO_dn)
	
	#initial state
	#State=np.kron(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(np.reshape(H12, (1, dup*ddn)))))
	State_up, State_dn=np.zeros(dup), np.zeros(ddn)
	State_up[indx2(up_atm, V_up)], State_dn[indx2(dn_atm, V_dn)]=1, 1
	State=np.outer(State_up, State_dn)
	#State=np.outer(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(H12)))
	State = State/np.sqrt(sum(sum(np.abs(State)**2)))
	#print sum(sum(np.abs(State)**2))
	#print NE_up/2
	#time evol
	T=np.linspace(ti, tf, n+1)
	dt=T[1]-T[0]
	#r=100
	dU_up=expm(-1j*H_up*dt/r)
	del(H_up)
	dU_dn=expm(-1j*H_dn*dt/r)
	del(H_dn)
	dU_ud=np.exp(-1j*H12*U*dt/r)
	del(H12)
	t2s=tm.time()
	#print('defn+exponentiation', t2s-t1s)
	
	#obswervables
	NE_up=np.outer( NE_up, np.ones(ddn))
	NE_dn=np.outer( np.ones(dup), NE_dn)
	P_up=[]
	P_dn=[]
	t_buff=0
	while t_buff<ti:
		t_buff=t_buff+dt/r
		State = np.dot(dU_up, State)
		State = np.dot(State, dU_dn)
		State=dU_ud*State
	
	#State=np.dot(Ui, State)
	#State2=np.transpose(State)[0]
	q_up=np.zeros(L)
	q_dn=np.zeros(L)
	rho_dn=sum(np.abs(State)**2)
	rho_up=sum(np.abs(State.transpose())**2)
	#print(dup, ddn, np.shape(State), np.shape(rho_up))
	for i in range(dup):
		for j in range(Nup):
			q_up[V_up[i][j]]=q_up[V_up[i][j]]+rho_up[i]
	for i in range(ddn):
		for j in range(Ndn):
			q_dn[V_dn[i][j]]=q_dn[V_dn[i][j]]+rho_dn[i]
	P_up.append(q_up)
	P_dn.append(q_dn)
	for ktevol in range(n):
		t2s=tm.time()
		#State=State-1j*dt*np.dot(H_up, State)-1j*dt*np.dot(State, H_dn)-1j*dt*U*H12*State
		#State = State/np.sqrt(sum(sum(np.abs(State)**2)))
		for sub_t in range(int(r)):
			State = dU_ud*np.dot(dU_up, np.dot(State, dU_dn))
			#State=dU_ud*multi_dot(dU_up, State, dU_dn)
			#State = dU_ud*np.dot(State, dU_dn)
			#State=dU_ud*State
		q_up=np.zeros(L)
		q_dn=np.zeros(L)
		rho_dn=sum(np.abs(State)**2)
		rho_up=sum(np.abs(State.transpose())**2)
		for i in range(dup):
			
			for j in range(Nup):
				q_up[V_up[i][j]]=q_up[V_up[i][j]]+rho_up[i]
		for i in range(ddn):
			for j in range(Ndn):
				q_dn[V_dn[i][j]]=q_dn[V_dn[i][j]]+rho_dn[i]
		P_up.append(q_up)
		P_dn.append(q_dn)
		#print('time step '+str(ktevol)+' of '+str(n)+' took '+ str(tm.time()-t2s) + ' s')
	del(dU_up)
	del(dU_dn)
	del(dU_ud)
	return np.array(T)/(2*np.pi), np.array(P_up), np.array(P_dn) 
	
	
def tevol_occupancy_new_exp(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, up_atm, dn_atm, U, ti, tf, n, r):
	t1s=tm.time()
	dup=int(comb(L, Nup))
	ddn=int(comb(L, Ndn))
	H_up1, H_dn1, H12, V_up, V_dn, NO_up, NO_dn=Hamiltonian(L,1,1, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U)
	del(H12)
	del(V_up)
	del(V_dn)
	del(NO_up)
	del(NO_dn)
	H_up, H_dn, H12, V_up, V_dn, NO_up, NO_dn=Hamiltonian(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U)
	del(H_up)
	del(H_dn)
	NE_up=Nup-np.array(NO_up)
	NE_dn=Ndn-np.array(NO_dn)
	
	#initial state
	#State=np.kron(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(np.reshape(H12, (1, dup*ddn)))))
	State_up, State_dn=np.zeros(dup), np.zeros(ddn)
	State_up[indx2(up_atm, V_up)], State_dn[indx2(dn_atm, V_dn)]=1, 1
	State=np.outer(State_up, State_dn)
	#State=np.outer(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(H12)))
	State = State/np.sqrt(sum(sum(np.abs(State)**2)))
	#print sum(sum(np.abs(State)**2))
	#print NE_up/2
	#time evol
	T=np.linspace(ti, tf, n+1)
	dt=T[1]-T[0]
	#r=100
	U1=expm(-1j*H_up1*dt/r)
	U2=expm(-1j*H_dn1*dt/r)
	dU_up=my_mat_exp(U1, Nup, V_up, dup )
	dU_dn=my_mat_exp(U2, Ndn, V_dn, ddn )
	
	dU_ud=np.exp(-1j*H12*U*dt/r)
	del(H12)
	t2s=tm.time()
	print('defn+exponentiation', t2s-t1s)
	
	#obswervables
	#NE_up=np.outer( NE_up, np.ones(ddn))
	#NE_dn=np.outer( np.ones(dup), NE_dn)
	NEUP=[]
	NOUP=[]
	NEDN=[]
	NODN=[]
	t_buff=0
	while t_buff<ti:
		t_buff=t_buff+dt/r
		State = np.dot(dU_up, State)
		State = np.dot(State, dU_dn)
		State=dU_ud*State
	
	#State=np.dot(Ui, State)
	#State2=np.transpose(State)[0]
	rho=np.zeros((dup, ddn))
	rho=np.maximum(np.abs(State)**2, rho)
	#NEUP.append(sum(sum(NE_up*np.abs(State)**2)))
	#NEDN.append(sum(sum(np.abs(State)**2*NE_dn)))
	#dU=[dU_up, dU_dn, dU_ud]
	for ktevol in range(n):
		t2s=tm.time()
		#State=State-1j*dt*np.dot(H_up, State)-1j*dt*np.dot(State, H_dn)-1j*dt*U*H12*State
		#State = State/np.sqrt(sum(sum(np.abs(State)**2)))
		for sub_t in range(int(r)):
			State = dU_ud*np.dot(dU_up, np.dot(State, dU_dn))
			#State=dU_ud*multi_dot(dU_up, State, dU_dn)
			#State = dU_ud*np.dot(State, dU_dn)
			#State=dU_ud*State
		#NEUP.append(sum(sum(NE_up*np.abs(State)**2)))
		#NEDN.append(sum(sum(np.abs(State)**2*NE_dn)))
		rho=np.maximum(np.abs(State)**2, rho)
		print('time step '+str(ktevol)+' of '+str(n)+' took '+ str(tm.time()-t2s) + ' s')
	del(dU_up)
	del(dU_dn)
	del(dU_ud)
	return rho
	
	
def tevol_full_state_trunc(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, up_atm, dn_atm, U, ti, tf, n, up_index, dn_index):
	t1s=tm.time()
	dup=int(comb(L, Nup))
	ddn=int(comb(L, Ndn))
	H=Hamiltonian_trunc(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, np.array(up_index), np.array(dn_index))
	
	
	#initial state
	State=np.zeros(len(dn_index), dtype=complex)
	State[0]=1
	T=np.linspace(ti, tf, n+1)
	dt=T[1]-T[0]
	print( 'defn done')
	#r=100
	dU=expm(-1j*H*dt)
	del(H)
	
	t2s=tm.time()
	print('defn+exponentiation', t2s-t1s)
	
	#obswervables
	
	State_out=[]
	t_buff=0
	while t_buff<ti:
		t_buff=t_buff+dt
		State = np.dot(dU, State)
		
	
	
	#print(dup, ddn, np.shape(State), np.shape(rho_up))
	State_out.append(np.abs(State)**2)
	for ktevol in range(n):
		t2s=tm.time()
		State = np.dot(dU, State)
		State_out.append(np.abs(State)**2)
		print('time step '+str(ktevol)+' of '+str(n)+' took '+ str(tm.time()-t2s) + ' s')
	return np.array(T)/(2*np.pi), np.array(State_out)
	
	
def tevol_full_state_trunc2(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, up_atm, dn_atm, U, ti, tf, n,  up_index, dn_index):
	t1s=tm.time()
	dup=int(comb(L, Nup))
	ddn=int(comb(L, Ndn))
	trunc_dim=len(up_index)
	State=np.zeros(len(dn_index), dtype=complex)
	State[0]=1
	
	H=Hamiltonian_trunc(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, np.array(up_index), np.array(dn_index))
	State_dot=-1j*np.dot(H, State)
	#del(H)
	print( 'derivative done')
	
	H2=Hamiltonian_trunc_k(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, np.array(up_index), np.array(dn_index))
	#print np.sum(np.abs(H-H2))
	Z=np.zeros((trunc_dim, trunc_dim))
	I=np.eye(trunc_dim)
	A=np.concatenate((Z, I), axis=1)
	B=np.concatenate((-H2, Z), axis=1)
	H=np.concatenate((A, B), axis=0)
	#initial state
	
	T=np.linspace(ti, tf, n+1)
	dt=T[1]-T[0]
	print ('defn done')
	#r=100
	dU=expm(H*dt)
	del(H)
	State=np.concatenate((State, State_dot))
	
	t2s=tm.time()
	print('defn+exponentiation', t2s-t1s)
	
	#obswervables
	
	State_out=[]
	t_buff=0
	while t_buff<ti:
		t_buff=t_buff+dt
		State = np.dot(dU, State)
		
	
	
	#print(dup, ddn, np.shape(State), np.shape(rho_up))
	State_out.append(np.abs(State[:trunc_dim])**2)
	for ktevol in range(n):
		t2s=tm.time()
		State = np.dot(dU, State)
		#State=State/np.sqrt(np.sum(np.abs(State[:trunc_dim])**2))
		State_out.append(np.abs(State[:trunc_dim])**2)
		print('time step '+str(ktevol)+' of '+str(n)+' took '+ str(tm.time()-t2s) + ' s')
	return np.array(T)/(2*np.pi), np.array(State_out)
	
def tevol_full_state_trunc2_temp(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, up_atm, dn_atm, U, ti, tf, n,  up_index, dn_index, i_cut, ord):
	t1s=tm.time()
	dup=int(comb(L, Nup))
	ddn=int(comb(L, Ndn))
	trunc_dim=i_cut
	i_cut=i_cut+100
	State=np.zeros(trunc_dim, dtype=complex)
	State[0]=1
	
	H1=Hamiltonian_trunc(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, np.array(up_index[:i_cut]), np.array(dn_index[:i_cut]))
	
	#del(H)
	print( 'derivative done')
	
	H2=Hamiltonian_trunc_k_test(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, np.array(up_index), np.array(dn_index), i_cut, 2)
	v, w=np.linalg.eigh(H2)
	#H1=np.dot(w, np.dot(np.diagflat(v**(1.0/2)), w.transpose()))
	print (v)
	#State_dot=-1j*np.dot(H1, State)
	#print np.sum(np.abs(H-H2))
	H2=np.dot(H1, H1)
	#Z=np.zeros((trunc_dim, trunc_dim))
	#I=np.eye(trunc_dim)
	#A=np.concatenate((Z, I), axis=1) 
	 
	#B=np.concatenate((-H2, Z), axis=1)
	#H=np.concatenate((A, B), axis=0)
	#initial state
	#v=np.abs(v)**(1.0/ord)*np.sign(v)
	#H=np.dot(w, np.dot(np.diagflat(v), w.transpose()))
	T=np.linspace(ti, tf, n+1)
	dt=T[1]-T[0]
	print( 'defn done')
	#print np.abs(v)**(1.0/3)*np.sign(v)
	#r=100
	#dU=expm(H*dt)
	r=15
	#dU=np.eye(i_cut)-1j*dt/(2**r)*H1-(dt/(2**r))**2*H2
	#for iter in range(r):
	#	dU=np.dot(dU, dU)
	dU=expm(-1j*dt*H1)[:trunc_dim, :trunc_dim]
	#del(H1)
	#State=np.concatenate((State, State_dot))
	
	t2s=tm.time()
	print('defn+exponentiation', t2s-t1s)
	
	#obswervables
	
	State_out=[]
	t_buff=0
	while t_buff<ti:
		t_buff=t_buff+dt
		State = np.dot(dU, State)
		
	
	 
	#print(dup, ddn, np.shape(State), np.shape(rho_up))
	State_out.append(np.abs(State[:trunc_dim])**2)
	for ktevol in range(n):
		t2s=tm.time()
		State = np.dot(dU, State)
		#State=State/np.sqrt(np.sum(np.abs(State[:trunc_dim])**2))
		State_out.append(np.abs(State[:trunc_dim])**2)
		print('time step '+str(ktevol)+' of '+str(n)+' took '+ str(tm.time()-t2s) + ' s')
	return np.array(T)/(2*np.pi), np.array(State_out), H2, H1
	
	
def tevol_full_state(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, up_atm, dn_atm, U, ti, tf, n, r):
	t1s=tm.time()
	dup=int(comb(L, Nup))
	ddn=int(comb(L, Ndn))
	H_up, H_dn, H12, V_up, V_dn, NO_up, NO_dn=Hamiltonian(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U)
	NE_up=Nup-np.array(NO_up)
	NE_dn=Ndn-np.array(NO_dn)
	eV_up, W_up= np.linalg.eigh(H_up)
	#del(eV_up)
	
	eV_dn, W_dn= np.linalg.eigh(H_dn)
	#initial state
	#State=np.kron(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(np.reshape(H12, (1, dup*ddn)))))
	State_up, State_dn=np.zeros(dup), np.zeros(ddn)
	State_up[indx2(up_atm, V_up)], State_dn[indx2(dn_atm, V_dn)]=1, 1
	State=np.outer(State_up, State_dn)
	#State=np.outer(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(H12)))
	State = State/np.sqrt(sum(sum(np.abs(State)**2)))
	#print sum(sum(np.abs(State)**2))
	#print NE_up/2
	#time evol
	T=np.linspace(ti, tf, n+1)
	dt=T[1]-T[0]
	#r=100
	dU_up=expm(-1j*H_up*dt/r)
	del(H_up)
	dU_dn=expm(-1j*H_dn*dt/r)
	del(H_dn)
	dU_ud=np.exp(-1j*H12*U*dt/r)
	del(H12)
	t2s=tm.time()
	#print('defn+exponentiation', t2s-t1s)
	
	#obswervables
	NE_up=np.outer( NE_up, np.ones(ddn))
	NE_dn=np.outer( np.ones(dup), NE_dn)
	P_up=[]
	P_dn=[]
	t_buff=0
	while t_buff<ti:
		t_buff=t_buff+dt/r
		State = np.dot(dU_up, State)
		State = np.dot(State, dU_dn)
		State=dU_ud*State
	
	#State=np.dot(Ui, State)
	#State2=np.transpose(State)[0]
	State_out=[]
	State_out_WSB=[]
	State_out.append(State)
	State_out_WSB.append(np.abs(np.dot(W_up.transpose(), np.dot(State, W_dn)))**2)
	q_up=np.zeros(L)
	q_dn=np.zeros(L)
	
	for ktevol in range(n):
		t2s=tm.time()
		#State=State-1j*dt*np.dot(H_up, State)-1j*dt*np.dot(State, H_dn)-1j*dt*U*H12*State
		#State = State/np.sqrt(sum(sum(np.abs(State)**2)))
		for sub_t in range(int(r)):
			State = dU_ud*np.dot(dU_up, np.dot(State, dU_dn))
			#State=dU_ud*multi_dot(dU_up, State, dU_dn)
			#State = dU_ud*np.dot(State, dU_dn)
			#State=dU_ud*State
		q_up=np.zeros(L)
		q_dn=np.zeros(L)
		State_out.append(State)
		#State_out_WSB.append(np.abs(np.dot(W_up.transpose(), np.dot(State, W_dn)))**2)
		#print('time step '+str(ktevol)+' of '+str(n)+' took '+ str(tm.time()-t2s) + ' s')
	del(dU_up)
	del(dU_dn)
	del(dU_ud)
	return T, State_out
	
def tevol_full_state_for_chebychev(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, up_atm, dn_atm, U,  tf, dt):
	t1s=tm.time()
	dup=int(comb(L, Nup))
	ddn=int(comb(L, Ndn))
	H_up, H_dn, H12, V_up, V_dn, NO_up, NO_dn=Hamiltonian(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U)
	NE_up=Nup-np.array(NO_up)
	NE_dn=Ndn-np.array(NO_dn)
	eV_up, W_up= np.linalg.eigh(H_up)
	#del(eV_up)
	
	eV_dn, W_dn= np.linalg.eigh(H_dn)
	#initial state
	#State=np.kron(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(np.reshape(H12, (1, dup*ddn)))))
	State_up, State_dn=np.zeros(dup), np.zeros(ddn)
	State_up[indx2(up_atm, V_up)], State_dn[indx2(dn_atm, V_dn)]=1, 1
	State=np.outer(State_up, State_dn)
	#State=np.outer(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(H12)))
	State = State/np.sqrt(sum(sum(np.abs(State)**2)))
	#print sum(sum(np.abs(State)**2))
	#print NE_up/2
	#time evol
	n=int(tf/dt)
	rem=tf-n*dt
	#r=100
	
	
	
	dU_up=expm(-1j*H_up*dt)
	#del(H_up)
	dU_dn=expm(-1j*H_dn*dt)
	#del(H_dn)
	dU_ud=np.exp(-1j*H12*U*dt)
	#del(H12)
	t2s=tm.time()
	#print('defn+exponentiation', t2s-t1s)
	
	#obswervables
	
		
	
	#State=np.dot(Ui, State)
	#State2=np.transpose(State)[0]
	
	t=rem
	for ktevol in range(n):
		t2s=tm.time()
		#State=State-1j*dt*np.dot(H_up, State)-1j*dt*np.dot(State, H_dn)-1j*dt*U*H12*State
		#State = State/np.sqrt(sum(sum(np.abs(State)**2)))
		State = dU_ud*np.dot(dU_up, np.dot(State, dU_dn))
		t=t+dt
		#State=dU_ud*multi_dot(dU_up, State, dU_dn)
		#State = dU_ud*np.dot(State, dU_dn)
		#State=dU_ud*State
		#q_up=np.zeros(L)
		#q_dn=np.zeros(L)
		#State_out.append(State)
		#State_out_WSB.append(np.abs(np.dot(W_up.transpose(), np.dot(State, W_dn)))**2)
		#print('time step '+str(ktevol)+' of '+str(n)+' took '+ str(tm.time()-t2s) + ' s')
	dU_up=expm(-1j*H_up*rem)
	dU_dn=expm(-1j*H_dn*rem)
	dU_ud=np.exp(-1j*H12*U*rem)
	State = np.dot(dU_up, State)
	State = np.dot(State, dU_dn)
	State=dU_ud*State
	del(dU_up)
	del(dU_dn)
	del(dU_ud)
	return State, rem
	
def tevol_Imb_for_chebychev(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, up_atm, dn_atm, U,  tf, dt):
	t1s=tm.time()
	dup=int(comb(L, Nup))
	ddn=int(comb(L, Ndn))
	H_up, H_dn, H12, V_up, V_dn, NO_up, NO_dn=Hamiltonian(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U)
	NE_up=Nup-np.array(NO_up)
	NE_dn=Ndn-np.array(NO_dn)
	eV_up, W_up= np.linalg.eigh(H_up)
	#del(eV_up)
	
	eV_dn, W_dn= np.linalg.eigh(H_dn)
	#initial state
	#State=np.kron(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(np.reshape(H12, (1, dup*ddn)))))
	State_up, State_dn=np.zeros(dup), np.zeros(ddn)
	State_up[indx2(up_atm, V_up)], State_dn[indx2(dn_atm, V_dn)]=1, 1
	State=np.outer(State_up, State_dn)
	#State=np.outer(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(H12)))
	State = State/np.sqrt(sum(sum(np.abs(State)**2)))
	#print sum(sum(np.abs(State)**2))
	#print NE_up/2
	#time evol
	n=int(tf/dt)
	rem=tf-n*dt
	#r=100
	
	
	
	dU_up=expm(-1j*H_up*dt)
	#del(H_up)
	dU_dn=expm(-1j*H_dn*dt)
	#del(H_dn)
	dU_ud=np.exp(-1j*H12*U*dt)
	#del(H12)
	t2s=tm.time()
	#print('defn+exponentiation', t2s-t1s)
	
	#obswervables
	NE_up=np.outer( NE_up, np.ones(ddn))
	NE_dn=np.outer( np.ones(dup), NE_dn)
	
		
	
	#State=np.dot(Ui, State)
	#State2=np.transpose(State)[0]
	
	t=rem
	for ktevol in range(n):
		t2s=tm.time()
		#State=State-1j*dt*np.dot(H_up, State)-1j*dt*np.dot(State, H_dn)-1j*dt*U*H12*State
		#State = State/np.sqrt(sum(sum(np.abs(State)**2)))
		State = dU_ud*np.dot(dU_up, np.dot(State, dU_dn))
		t=t+dt
		#State=dU_ud*multi_dot(dU_up, State, dU_dn)
		#State = dU_ud*np.dot(State, dU_dn)
		#State=dU_ud*State
		#q_up=np.zeros(L)
		#q_dn=np.zeros(L)
		#State_out.append(State)
		#State_out_WSB.append(np.abs(np.dot(W_up.transpose(), np.dot(State, W_dn)))**2)
		#print('time step '+str(ktevol)+' of '+str(n)+' took '+ str(tm.time()-t2s) + ' s')
	dU_up=expm(-1j*H_up*rem)
	dU_dn=expm(-1j*H_dn*rem)
	dU_ud=np.exp(-1j*H12*U*rem)
	State = np.dot(dU_up, State)
	State = np.dot(State, dU_dn)
	State=dU_ud*State
	NEUP=sum(sum(NE_up*np.abs(State)**2))
	NEDN=sum(sum(np.abs(State)**2*NE_dn))
	
	del(dU_up)
	del(dU_dn)
	del(dU_ud)
	return NEUP, NEDN
	
def tevol_full_state_WSB(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, up_atm, dn_atm, U, ti, tf, n, r):
	t1s=tm.time()
	dup=int(comb(L, Nup))
	ddn=int(comb(L, Ndn))
	H_up, H_dn, H12, V_up, V_dn, NO_up, NO_dn=Hamiltonian(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U)
	NE_up=Nup-np.array(NO_up)
	NE_dn=Ndn-np.array(NO_dn)
	eV_up, W_up= np.linalg.eigh(H_up)
	#del(eV_up)
	
	eV_dn, W_dn= np.linalg.eigh(H_dn)
	#initial state
	#State=np.kron(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(np.reshape(H12, (1, dup*ddn)))))
	State_up, State_dn=np.zeros(dup), np.zeros(ddn)
	State_up[indx2(up_atm, V_up)], State_dn[indx2(dn_atm, V_dn)]=1, 1
	#State_up=np.dot(W_up.transpose(), State_up)
	#i_up=np.argmax(np.abs(State_up))
	#State_dn=np.dot(W_dn.transpose(), State_dn)
	#i_dn=np.argmax(np.abs(State_dn))
	#State_up, State_dn=np.zeros(dup), np.zeros(ddn)
	#State_up[i_up], State_dn[i_dn]=1, 1
	State_up=np.dot(W_up, State_up)
	State_dn=np.dot(W_dn, State_dn)
	
	State=np.outer(State_up, State_dn)
	#State=np.outer(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(H12)))
	State = State/np.sqrt(sum(sum(np.abs(State)**2)))
	#print sum(sum(np.abs(State)**2))
	#print NE_up/2
	#time evol
	T=np.linspace(ti, tf, n+1)
	dt=T[1]-T[0]
	#r=100
	dU_up=expm(-1j*H_up*dt/r)
	del(H_up)
	dU_dn=expm(-1j*H_dn*dt/r)
	del(H_dn)
	dU_ud=np.exp(-1j*H12*U*dt/r)
	#del(H12)
	t2s=tm.time()
	#print('defn+exponentiation', t2s-t1s)
	
	#obswervables
	NE_up=np.outer( NE_up, np.ones(ddn))
	NE_dn=np.outer( np.ones(dup), NE_dn)
	P_up=[]
	P_dn=[]
	t_buff=0
	while t_buff<ti:
		t_buff=t_buff+dt/r
		State = np.dot(dU_up, State)
		State = np.dot(State, dU_dn)
		State=dU_ud*State
	
	#State=np.dot(Ui, State)
	#State2=np.transpose(State)[0]
	State_out=[]
	State_out_WSB=[]
	State_out.append(np.abs(State)**2)
	State_out_WSB.append(np.abs(np.dot(W_up.transpose(), np.dot(State, W_dn)))**2)
	q_up=np.zeros(L)
	q_dn=np.zeros(L)
	
	for ktevol in range(n):
		t2s=tm.time()
		#State=State-1j*dt*np.dot(H_up, State)-1j*dt*np.dot(State, H_dn)-1j*dt*U*H12*State
		#State = State/np.sqrt(sum(sum(np.abs(State)**2)))
		for sub_t in range(int(r)):
			State = dU_ud*np.dot(dU_up, np.dot(State, dU_dn))
			#State=dU_ud*multi_dot(dU_up, State, dU_dn)
			#State = dU_ud*np.dot(State, dU_dn)
			#State=dU_ud*State
		q_up=np.zeros(L)
		q_dn=np.zeros(L)
		State_out.append(np.abs(State)**2)
		State_out_WSB.append(np.abs(np.dot(W_up.transpose(), np.dot(State, W_dn)))**2)
		print('time step '+str(ktevol)+' of '+str(n)+' took '+ str(tm.time()-t2s) + ' s')
	del(dU_up)
	del(dU_dn)
	del(dU_ud)
	return V_up, V_dn, State_out, State_out_WSB, eV_up, eV_dn, W_up, W_dn, H12
	

	
	
def tevol_occupancy_new(Lup, Ldn, Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, up_atm, dn_atm, U, ti, tf, n, r, sw):
	t1s=tm.time()
	#dup=int(comb(L, Nup))
	#ddn=int(comb(L, Ndn))
	L=max(Lup, Ldn)
	#H_up, H_dn, H12, V_up, V_dn, NO_up, NO_dn=Hamiltonian_new(Lup, Ldn,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, up_atm, dn_atm, sw)
	H_up, H_dn, H12, Hdiag_up, Hdiag_dn, V_up, V_dn, NO_up, NO_dn=Hamiltonian_new_hop_sep(Lup, Ldn,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, up_atm, dn_atm, sw)
	NE_up=Nup-np.array(NO_up)
	NE_dn=Ndn-np.array(NO_dn)
	dup=len(NO_up)
	ddn=len(NO_dn)
	offset=L/2+1-min(min(up_atm), min(dn_atm))
	up_atm=offset+up_atm
	dn_atm=offset+dn_atm
	L2=L/2+1+max(max(up_atm), max(dn_atm))
	
	#initial state
	#State=np.kron(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(np.reshape(H12, (1, dup*ddn)))))
	State_up, State_dn=np.zeros(dup), np.zeros(ddn)
	State_up[indx2(up_atm, V_up)], State_dn[indx2(dn_atm, V_dn)]=1, 1
	State=np.outer(State_up, State_dn)
	#State=np.outer(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(H12)))
	State = State/np.sqrt(sum(sum(np.abs(State)**2)))
	#print sum(sum(np.abs(State)**2))
	#print NE_up/2
	#time evol
	T=np.linspace(ti, tf, n+1)
	dt=T[1]-T[0]
	#r=100
	dU_up=expm(-1j*H_up*dt/r)
	del(H_up)
	dU_dn=expm(-1j*H_dn*dt/r)
	del(H_dn)
	dU_ud=np.exp(-1j*H12*U*dt/r)
	del(H12)
	dU_ud=dU_ud*np.outer(np.exp(-1j*dt/r*Hdiag_up), np.exp(-1j*dt/r*Hdiag_dn))
	t2s=tm.time()
	print('defn+exponentiation', t2s-t1s)
	
	#obswervables
	NE_up=np.outer( NE_up, np.ones(ddn))
	NE_dn=np.outer( np.ones(dup), NE_dn)
	P_up=[]
	P_dn=[]
	t_buff=0
	while t_buff<ti:
		t_buff=t_buff+dt/r
		State = np.dot(dU_up, State)
		State = np.dot(State, dU_dn)
		State=dU_ud*State
	
	#State=np.dot(Ui, State)
	#State2=np.transpose(State)[0]
	q_up=np.zeros(L2)
	q_dn=np.zeros(L2)
	rho_dn=sum(np.abs(State)**2)
	rho_up=sum(np.abs(State.transpose())**2)
	#print(dup, ddn, np.shape(State), np.shape(rho_up))
	for i in range(dup):
		for j in range(Nup):
			q_up[V_up[i][j]]=q_up[V_up[i][j]]+rho_up[i]
	for i in range(ddn):
		for j in range(Ndn):
			q_dn[V_dn[i][j]]=q_dn[V_dn[i][j]]+rho_dn[i]
	P_up.append(q_up)
	P_dn.append(q_dn)
	for ktevol in range(n):
		
		t2s=tm.time()
		#State=State-1j*dt*np.dot(H_up, State)-1j*dt*np.dot(State, H_dn)-1j*dt*U*H12*State
		#State = State/np.sqrt(sum(sum(np.abs(State)**2)))
		for sub_t in range(int(r)):
			State = dU_ud*np.dot(dU_up, np.dot(State, dU_dn))
			#State=dU_ud*multi_dot(dU_up, State, dU_dn)
			#State = dU_ud*np.dot(State, dU_dn)
			#State=dU_ud*State
		q_up=np.zeros(L2)
		q_dn=np.zeros(L2)
		rho_dn=sum(np.abs(State)**2)
		rho_up=sum(np.abs(State.transpose())**2)
		#for i in range(dup):
			
		#	for j in range(Nup):
		#		q_up[V_up[i][j]]=q_up[V_up[i][j]]+rho_up[i]
		#for i in range(ddn):
		#	for j in range(Ndn):
		#		q_dn[V_dn[i][j]]=q_dn[V_dn[i][j]]+rho_dn[i]
		#P_up.append(q_up)
		#P_dn.append(q_dn)
		print('time step '+str(ktevol)+' of '+str(n)+' took '+ str(tm.time()-t2s) + ' s')
	del(dU_up)
	del(dU_dn)
	del(dU_ud)
	return   V_up, V_dn, State
	
	
def tevol_fock_space_density(Lup, Ldn, Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, up_atm, dn_atm, U, ti, tf, n, r, sw):
	t1s=tm.time()
	#dup=int(comb(L, Nup))
	#ddn=int(comb(L, Ndn))
	L=max(Lup, Ldn)
	#H_up, H_dn, H12, V_up, V_dn, NO_up, NO_dn=Hamiltonian_new(Lup, Ldn,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, up_atm, dn_atm, sw)
	H_up, H_dn, H12, Hdiag_up, Hdiag_dn, V_up, V_dn, NO_up, NO_dn=Hamiltonian_new_hop_sep(Lup, Ldn,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, up_atm, dn_atm, sw)
	NE_up=Nup-np.array(NO_up)
	NE_dn=Ndn-np.array(NO_dn)
	dup=len(NO_up)
	ddn=len(NO_dn)
	offset=L/2+1-min(min(up_atm), min(dn_atm))
	up_atm=offset+up_atm
	dn_atm=offset+dn_atm
	L2=L/2+1+max(max(up_atm), max(dn_atm))
	
	#initial state
	#State=np.kron(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(np.reshape(H12, (1, dup*ddn)))))
	State_up, State_dn=np.zeros(dup), np.zeros(ddn)
	State_up[indx2(up_atm, V_up)], State_dn[indx2(dn_atm, V_dn)]=1, 1
	State=np.outer(State_up, State_dn)
	#State=np.outer(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(H12)))
	State = State/np.sqrt(sum(sum(np.abs(State)**2)))
	#print sum(sum(np.abs(State)**2))
	#print NE_up/2
	#time evol
	T=np.linspace(ti, tf, n+1)
	dt=T[1]-T[0]
	#r=100
	dU_up=expm(-1j*H_up*dt/r)
	del(H_up)
	dU_dn=expm(-1j*H_dn*dt/r)
	del(H_dn)
	dU_ud=np.exp(-1j*H12*U*dt/r)
	del(H12)
	dU_ud=dU_ud*np.outer(np.exp(-1j*dt/r*Hdiag_up), np.exp(-1j*dt/r*Hdiag_dn))
	t2s=tm.time()
	print('defn+exponentiation', t2s-t1s)
	
	#obswervables
	NE_up=np.outer( NE_up, np.ones(ddn))
	NE_dn=np.outer( np.ones(dup), NE_dn)
	P_up=[]
	P_dn=[]
	t_buff=0
	while t_buff<ti:
		t_buff=t_buff+dt/r
		State = np.dot(dU_up, State)
		State = np.dot(State, dU_dn)
		State=dU_ud*State
	
	#State=np.dot(Ui, State)
	#State2=np.transpose(State)[0]
	State_out=[]
	q_up=np.zeros(L2)
	q_dn=np.zeros(L2)
	rho_dn=sum(np.abs(State)**2)
	rho_up=sum(np.abs(State.transpose())**2)
	#print(dup, ddn, np.shape(State), np.shape(rho_up))
	for i in range(dup):
		for j in range(Nup):
			q_up[V_up[i][j]]=q_up[V_up[i][j]]+rho_up[i]
	for i in range(ddn):
		for j in range(Ndn):
			q_dn[V_dn[i][j]]=q_dn[V_dn[i][j]]+rho_dn[i]
	P_up.append(q_up)
	P_dn.append(q_dn)
	State_out.append(np.abs(State)**2)
	for ktevol in range(n):
		
		t2s=tm.time()
		#State=State-1j*dt*np.dot(H_up, State)-1j*dt*np.dot(State, H_dn)-1j*dt*U*H12*State
		#State = State/np.sqrt(sum(sum(np.abs(State)**2)))
		for sub_t in range(int(r)):
			State = dU_ud*np.dot(dU_up, np.dot(State, dU_dn))
			#State=dU_ud*multi_dot(dU_up, State, dU_dn)
			#State = dU_ud*np.dot(State, dU_dn)
			#State=dU_ud*State
		q_up=np.zeros(L2)
		q_dn=np.zeros(L2)
		State_out.append(np.abs(State)**2)
		#rho_dn=sum(np.abs(State)**2)
		#rho_up=sum(np.abs(State.transpose())**2)
		#for i in range(dup):
			
		#	for j in range(Nup):
		#		q_up[V_up[i][j]]=q_up[V_up[i][j]]+rho_up[i]
		#for i in range(ddn):
		#	for j in range(Ndn):
		#		q_dn[V_dn[i][j]]=q_dn[V_dn[i][j]]+rho_dn[i]
		#P_up.append(q_up)
		#P_dn.append(q_dn)
		print('time step '+str(ktevol)+' of '+str(n)+' took '+ str(tm.time()-t2s) + ' s')
	del(dU_up)
	del(dU_dn)
	del(dU_ud)
	return   V_up, V_dn, State_out
	
	
def tevol_fock_space_density_WSB(Lup, Ldn, Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, up_atm, dn_atm, U, ti, tf, n, r, sw):
	t1s=tm.time()
	#dup=int(comb(L, Nup))
	#ddn=int(comb(L, Ndn))
	L=max(Lup, Ldn)
	#H_up, H_dn, H12, V_up, V_dn, NO_up, NO_dn=Hamiltonian_new(Lup, Ldn,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, up_atm, dn_atm, sw)
	H_up, H_dn, H12, Hdiag_up, Hdiag_dn, V_up, V_dn, NO_up, NO_dn=Hamiltonian_new_hop_sep(Lup, Ldn,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, up_atm, dn_atm, sw)
	NE_up=Nup-np.array(NO_up)
	NE_dn=Ndn-np.array(NO_dn)
	dup=len(NO_up)
	ddn=len(NO_dn)
	offset=L/2+1-min(min(up_atm), min(dn_atm))
	up_atm=offset+up_atm
	dn_atm=offset+dn_atm
	L2=L/2+1+max(max(up_atm), max(dn_atm))
	
	#initial state
	#State=np.kron(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(np.reshape(H12, (1, dup*ddn)))))
	State_up, State_dn=np.zeros(dup), np.zeros(ddn)
	State_up[indx2(up_atm, V_up)], State_dn[indx2(dn_atm, V_dn)]=1, 1
	State=np.outer(State_up, State_dn)
	#State=np.outer(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(H12)))
	State = State/np.sqrt(sum(sum(np.abs(State)**2)))
	#print sum(sum(np.abs(State)**2))
	#print NE_up/2
	#time evol
	T=np.linspace(ti, tf, n+1)
	dt=T[1]-T[0]
	eV_up, W_up= np.linalg.eigh(H_up+np.diagflat(Hdiag_up))
	#del(eV_up)
	
	eV_dn, W_dn= np.linalg.eigh(H_dn+np.diagflat(Hdiag_dn))
	#del(eV_dn)
	
	#r=100
	dU_up=expm(-1j*H_up*dt/r)
	del(H_up)
	dU_dn=expm(-1j*H_dn*dt/r)
	del(H_dn)
	dU_ud=np.exp(-1j*H12*U*dt/r)
	del(H12)
	dU_ud=dU_ud*np.outer(np.exp(-1j*dt/r*Hdiag_up), np.exp(-1j*dt/r*Hdiag_dn))
	t2s=tm.time()
	print('defn+exponentiation', t2s-t1s)
	#H_up, H_dn, H12, V_up, V_dn, NO_up, NO_dn=Hamiltonian_new(Lup, Ldn,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, up_atm, dn_atm, sw)
	
	
	#obswervables
	NE_up=np.outer( NE_up, np.ones(ddn))
	NE_dn=np.outer( np.ones(dup), NE_dn)
	P_up=[]
	P_dn=[]
	t_buff=0
	while t_buff<ti:
		t_buff=t_buff+dt/r
		State = np.dot(dU_up, State)
		State = np.dot(State, dU_dn)
		State=dU_ud*State
	
	#State=np.dot(Ui, State)
	#State2=np.transpose(State)[0]
	State_out=[]
	State_out_WSB=[]
	q_up=np.zeros(L2)
	q_dn=np.zeros(L2)
	rho_dn=sum(np.abs(State)**2)
	rho_up=sum(np.abs(State.transpose())**2)
	#print(dup, ddn, np.shape(State), np.shape(rho_up))
	for i in range(dup):
		for j in range(Nup):
			q_up[V_up[i][j]]=q_up[V_up[i][j]]+rho_up[i]
	for i in range(ddn):
		for j in range(Ndn):
			q_dn[V_dn[i][j]]=q_dn[V_dn[i][j]]+rho_dn[i]
	P_up.append(q_up)
	P_dn.append(q_dn)
	State_out.append(np.abs(State)**2)
	State_out_WSB.append(np.abs(np.dot(W_up.transpose(), np.dot(State, W_dn)))**2)
	for ktevol in range(n):
		
		t2s=tm.time()
		#State=State-1j*dt*np.dot(H_up, State)-1j*dt*np.dot(State, H_dn)-1j*dt*U*H12*State
		#State = State/np.sqrt(sum(sum(np.abs(State)**2)))
		for sub_t in range(int(r)):
			State = dU_ud*np.dot(dU_up, np.dot(State, dU_dn))
			#State=dU_ud*multi_dot(dU_up, State, dU_dn)
			#State = dU_ud*np.dot(State, dU_dn)
			#State=dU_ud*State
		q_up=np.zeros(L2)
		q_dn=np.zeros(L2)
		State_out.append(np.abs(State)**2)
		State_out_WSB.append(np.abs(np.dot(W_up.transpose(), np.dot(State, W_dn)))**2)
		#rho_dn=sum(np.abs(State)**2)
		#rho_up=sum(np.abs(State.transpose())**2)
		#for i in range(dup):
			
		#	for j in range(Nup):
		#		q_up[V_up[i][j]]=q_up[V_up[i][j]]+rho_up[i]
		#for i in range(ddn):
		#	for j in range(Ndn):
		#		q_dn[V_dn[i][j]]=q_dn[V_dn[i][j]]+rho_dn[i]
		#P_up.append(q_up)
		#P_dn.append(q_dn)
		print('time step '+str(ktevol)+' of '+str(n)+' took '+ str(tm.time()-t2s) + ' s')
	del(dU_up)
	del(dU_dn)
	del(dU_ud)
	return   V_up, V_dn, State_out, State_out_WSB, eV_up, eV_dn
	
	
def tevol_with_avg(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, ti, tf, n, r, State_up, State_dn):
	t1s=tm.time()
	dup=int(comb(L, Nup))
	ddn=int(comb(L, Ndn))
	H_up, H_dn, H12, V_up, V_dn, NO_up, NO_dn=Hamiltonian(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U)
	NE_up=Nup-np.array(NO_up)
	NE_dn=Ndn-np.array(NO_dn)
	
	#initial state
	#State=np.kron(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(np.reshape(H12, (1, dup*ddn)))))
	#State=np.outer(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(H12)))
	iup=indx2(State_up, V_up)
	idn=indx2(State_dn, V_dn)
	State=np.zeros((dup, ddn))
	State[iup, idn]=1
	State = State/np.sqrt(sum(sum(np.abs(State)**2)))
	#print sum(sum(np.abs(State)**2))
	#print NE_up/2
	#time evol
	T=np.linspace(ti, tf, n+1)
	dt=T[1]-T[0]
	#r=100
	dU_up=expm(-1j*H_up*dt/r)
	del(H_up)
	dU_dn=expm(-1j*H_dn*dt/r)
	del(H_dn)
	dU_ud=np.exp(-1j*H12*U*dt/r)
	del(H12)
	t2s=tm.time()
	print('defn+exponentiation', t2s-t1s)
	
	#obswervables
	NE_up=np.outer( NE_up, np.ones(ddn))
	NE_dn=np.outer( np.ones(dup), NE_dn)
	NEUP=[]
	NOUP=[]
	NEDN=[]
	NODN=[]
	t_buff=0
	while t_buff<ti:
		t_buff=t_buff+dt/r
		State = np.dot(dU_up, State)
		State = np.dot(State, dU_dn)
		State=dU_ud*State
	#State=np.dot(Ui, State)
	#State2=np.transpose(State)[0]
	NEUP.append(sum(sum(NE_up*np.abs(State)**2)))
	NEDN.append(sum(sum(np.abs(State)**2*NE_dn)))
	#dU=[dU_up, dU_dn, dU_ud]
	for ktevol in range(n):
		print('time step', tm.time()-t2s)
		t2s=tm.time()
		#State=State-1j*dt*np.dot(H_up, State)-1j*dt*np.dot(State, H_dn)-1j*dt*U*H12*State
		#State = State/np.sqrt(sum(sum(np.abs(State)**2)))
		for sub_t in range(r):
			State = dU_ud*np.dot(dU_up, np.dot(State, dU_dn))
			#State=dU_ud*multi_dot(dU_up, State, dU_dn)
			#State = dU_ud*np.dot(State, dU_dn)
			#State=dU_ud*State
		
		
		
		NEUP.append(sum(sum(NE_up*np.abs(State)**2)))
		NEDN.append(sum(sum(np.abs(State)**2*NE_dn)))
	del(dU_up)
	del(dU_dn)
	del(dU_ud)
	return np.array(T)/(2*np.pi), np.array(NEUP), np.array(NEDN)
	
	
def tevol_fancy(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, ti, tf, n, r, it_num):
	t1s=tm.time()
	dup=int(comb(L, Nup))
	ddn=int(comb(L, Ndn))
	H_up, H_dn, H12, V_up, V_dn, NO_up, NO_dn=Hamiltonian(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U)
	NE_up=Nup-np.array(NO_up)
	NE_dn=Ndn-np.array(NO_dn)
	indices=np.array([n*(r+np.arange(it_num))])
	
	
	#initial state
	State=np.outer(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(H12)))
	State = State/np.sqrt(sum(sum(np.abs(State)**2)))
	State_list=[]
	for j in range(it_num):
		State_list.append(State+0j)
	State_list=np.array(State_list)
	#time evol
	T=np.linspace(ti, tf, n+1)
	dt=T[1]-T[0]

	#unitaries
	dU_up=[]
	for j in range(it_num):
		dU_up.append(expm(-1j*H_up*dt/(r+j)))
	del(H_up)
	dU_dn=[]
	for j in range(it_num):
		dU_dn.append(expm(-1j*H_dn*dt/(r+j)))
	del(H_dn)
	dU_ud=[]
	for j in range(it_num):
		dU_ud.append(np.exp(-1j*H12*U*dt/(r+j)))
	del(H12)
	t2s=tm.time()
	print('defn+exponentiation', t2s-t1s)
	
	#obswervables
	NE_up=np.outer( NE_up, np.ones(ddn))
	NE_dn=np.outer( np.ones(dup), NE_dn)
	NEUP=[]
	NOUP=[]
	NEDN=[]
	NODN=[]
	t_buff=0
	for j in range(it_num):
		while t_buff<ti:
			t_buff=t_buff+dt/(r+j)
			State_list[j] = np.dot(dU_up[j], State_list[j])
			State_list[j] = np.dot(State_list[j], dU_dn[j])
			State_list[j]=dU_ud[j]*State_list[j]
	State=convergence_reduce(State_list, indices)
	NEUP.append(sum(sum(NE_up*np.abs(State)**2)))
	NEDN.append(sum(sum(np.abs(State)**2*NE_dn)))
	State_list=[]
	for j in range(it_num):
		State_list.append(State)
	State_list=np.array(State_list)
	#dU=[dU_up, dU_dn, dU_ud]
	for ktevol in range(n):
		print('time step', tm.time()-t2s)
		t2s=tm.time()
		#State=State-1j*dt*np.dot(H_up, State)-1j*dt*np.dot(State, H_dn)-1j*dt*U*H12*State
		#State = State/np.sqrt(sum(sum(np.abs(State)**2)))
		for j in range(it_num):
			for sub_t in range(r+j):
				State_list[j] = np.dot(dU_up[j], State_list[j])
				State_list[j] = np.dot(State_list[j], dU_dn[j])
				State_list[j]=dU_ud[j]*State_list[j]
		
		State=convergence_reduce(State_list, indices)
		State = State/np.sqrt(sum(sum(np.abs(State)**2)))
		NEUP.append(sum(sum(NE_up*np.abs(State)**2)))
		NEDN.append(sum(sum(np.abs(State)**2*NE_dn)))
		State_list=[]
		for j in range(it_num):
			State_list.append(State)
		State_list=np.array(State_list)
	del(dU_up)
	del(dU_dn)
	del(dU_ud)
	return np.array(T)/(2*np.pi), np.array(NEUP), np.array(NEDN)  
 
	
def diag_ensemble(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U):
	dup=int(comb(L, Nup))
	ddn=int(comb(L, Ndn))
	H, V_up, V_dn, H12, NO_up, NO_dn=Hamiltonian(L,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U)
	NE_up=Nup-np.array(NO_up)
	NE_dn=Ndn-np.array(NO_dn)
	#print np.shape(NE_up), np.shape(np.kron(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)), np.shape((1-np.sign(np.reshape(H12, (1, dup*ddn)))))
	
	#initial state
	State=np.kron(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)*np.array((1-np.sign(np.reshape(H12, (1, dup*ddn)))))
	State = State/np.sqrt(sum(np.abs(State[0])**2))
	W, V = np.linalg.eig(H)
	del(H)
	del(W)
	# obsdevable
	NE_up=np.transpose(np.kron(NE_up, np.ones(ddn)))
	NE_dn=np.transpose(np.kron( np.ones(dup), NE_dn))
	NEUP=0
	NEDN=0
	for jstate in range(dup*ddn):
		NEUP=NEUP+np.abs(sum(State[0]*V[:,jstate]))**2*sum(NE_up*np.abs(V[:,jstate])**2)
		NEDN=NEDN+np.abs(sum(State[0]*V[:,jstate]))**2*sum(NE_dn*np.abs(V[:,jstate])**2)
	del(V)
		
	return NEUP, NEDN
	
def diag_ensemble_new(Lup, Ldn,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, up_atm, dn_atm, sw):
	L=max(Lup, Ldn)
	offset=L/2+1-min(min(up_atm), min(dn_atm))
	up_atm2=offset+up_atm
	dn_atm2=offset+dn_atm
	L2=L/2+1+max(max(up_atm2), max(dn_atm2))
	print( offset, L2, Lup, Ldn)
	H_up, H_dn, H12, V_up, V_dn, NO_up, NO_dn=Hamiltonian_new(Lup, Ldn, Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, up_atm, dn_atm, sw)
	NE_up=Nup-np.array(NO_up)
	NE_dn=Ndn-np.array(NO_dn)
	dup=len(NO_up)
	ddn=len(NO_dn)
	H=np.kron(H_up, np.eye(ddn))+np.kron(np.eye(dup), H_dn)+U*np.diagflat(np.reshape(H12,(1, dup*ddn) ))
	del(H_up)
	del(H_dn)
	del(H12)
	#print np.shape(NE_up), np.shape(np.kron(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)), np.shape((1-np.sign(np.reshape(H12, (1, dup*ddn)))))
	W, V = np.linalg.eigh(H)
	del(H)
	del(W)
	
	#initial state
	State_up, State_dn=np.zeros(dup), np.zeros(ddn)
	State_up[indx2(up_atm2, V_up)], State_dn[indx2(dn_atm2, V_dn)]=1, 1
	State=np.kron(State_up, State_dn)
	State = State/np.sqrt(np.sum(np.abs(State)**2))
	
	# obsdevable
	NE_up=np.transpose(np.kron(NE_up, np.ones(ddn)))
	NE_dn=np.transpose(np.kron( np.ones(dup), NE_dn))
	NEUP=0
	NEDN=0
	for jstate in range(dup*ddn):
		NEUP=NEUP+np.abs(sum(State*V[:,jstate]))**2*sum(NE_up*np.abs(V[:,jstate])**2)
		NEDN=NEDN+np.abs(sum(State*V[:,jstate]))**2*sum(NE_dn*np.abs(V[:,jstate])**2)
	del(V)
		
	return NEUP, NEDN, dup, ddn
	
	
def diag_ensemble_density_new(Lup, Ldn,Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, up_atm, dn_atm, sw):
	L=max(Lup, Ldn)
	offset=L/2+1-min(min(up_atm), min(dn_atm))
	up_atm2=offset+up_atm
	dn_atm2=offset+dn_atm
	L2=L/2+1+max(max(up_atm2), max(dn_atm2))
	print (offset, L2, Lup, Ldn)
	H_up, H_dn, H12, V_up, V_dn, NO_up, NO_dn=Hamiltonian_new(Lup, Ldn, Nup,Ndn, J, DeltaRR, DeltaAA, phiAA, DeltaS1, DeltaS2, alpha, U, up_atm, dn_atm, sw)
	NE_up=Nup-np.array(NO_up)
	NE_dn=Ndn-np.array(NO_dn)
	dup=len(NO_up)
	ddn=len(NO_dn)
	H=np.kron(H_up, np.eye(ddn))+np.kron(np.eye(dup), H_dn)+U*np.diagflat(np.reshape(H12,(1, dup*ddn) ))
	del(H_up)
	del(H_dn)
	del(H12)
	#print np.shape(NE_up), np.shape(np.kron(np.array(NE_up)/Nup, np.array(NE_dn)/Ndn)), np.shape((1-np.sign(np.reshape(H12, (1, dup*ddn)))))
	W, V = np.linalg.eigh(H)
	del(H)
	del(W)
	
	#initial state
	State_up, State_dn=np.zeros(dup), np.zeros(ddn)
	State_up[indx2(up_atm2, V_up)], State_dn[indx2(dn_atm2, V_dn)]=1, 1
	State=np.kron(State_up, State_dn)
	State = State/np.sqrt(np.sum(np.abs(State)**2))
	
	# obsdevable
	#P_up=np.transpose(np.kron(NE_up, np.ones(ddn)))
	#NE_dn=np.transpose(np.kron( np.ones(dup), NE_dn))
	P_up=0

	for jstate in range(dup*ddn):
		State_temp=V[:, jstate].reshape(dup, ddn)
		#State_temp=State.reshape(dup, ddn)
		q=np.sum(np.abs(State_temp)**2, axis=1)
		P_up=P_up+q*np.abs(sum(State*V[:,jstate]))**2
		#NEUP=NEUP+np.abs(sum(State*V[:,jstate]))**2*sum(NE_up*np.abs(V[:,jstate])**2)
		#NEDN=NEDN+np.abs(sum(State*V[:,jstate]))**2*sum(NE_dn*np.abs(V[:,jstate])**2)
	del(V)
	State_temp=State.reshape(dup, ddn)
		
	return np.sum(np.abs(State_temp)**2, axis=1), P_up,  dup, ddn, offset
	
	
def H_space_dim_estimate(Lup, Ldn, Nup, Ndn, up_atm, dn_atm, sw):
	L=max(Lup, Ldn)
	offset=L/2+1-min(min(up_atm), min(dn_atm))
	up_atm=offset+up_atm
	dn_atm=offset+dn_atm
	L2=L/2+1+max(max(up_atm), max(dn_atm))
	H_up, H_dn, H12, V_up, V_dn, NO_up, NO_dn=Hamiltonian_new(Lup, Ldn, Nup,Ndn, 0, 0, 0, 0, 0, 0, 0, 0, up_atm, dn_atm, sw)
	return len(NO_up), len(NO_dn)
