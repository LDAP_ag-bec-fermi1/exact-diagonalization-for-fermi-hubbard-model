# -*- coding: utf-8 -*-
"""
Created on Mon Jun 29 13:06:37 2020

@author: Bharath.Hebbe
"""



import numpy as np
import time as tm
import matplotlib.pyplot as plt
import Exact_Diagonalization as ED
import pickle

"""""""""""""""""""""""""""""""""""""""""""""""
SECTION  1: PARAMETERS IN THE HAMILTONIAN

"""""""""""""""""""""""""""""""""""""""""""""


##############################DYNAMICAL  PARAMETERS#################################################################################################################################################################
J=1.0				    # Tunneling rate in KHz
Delta_random=0			# Random disorder strength in KHz
Delta_aubry_andre=0		# Aubry-Andre disorder strength in KHz
phi_aubry_andre= 0		# Aubry-Andre disorder phase
Delta_dn=3*J			# Stark tilt for spin down in KHz
Delta_up=3*J			# Stark tilt for spin up in KHz
U=5*J					# Interaction in KHz
ti = 0					# Starting time in ms
tf =1					# Ending time in ms
n=100					# Number of datapoints in time the larger the better, the smaller the faster
Tr=7.51					# Revival time of Bloch oscillations
alpha=0.0
##############################################################################################################################################################################################

"""""""""""""""""""""""""""""""""""""""""""""""
SECTION  2: SYSTEM SIZE AND INITIAL STATE

"""""""""""""""""""""""""""""""""""""""""""""



L=21                        #System size
up_atm=np.arange(1, L, 4)   #Initial state: an array containg the lattice sites occupied by spin-up atoms
dn_atm=np.arange(3, L, 4)   #Initial state: an array containg the lattice sites occupied by spin-down atoms


###########################################################################################################

"""""""""""""""""""""""""""""""""""""""""""""""
SECTION  3: TIME EVOLUTION

"""""""""""""""""""""""""""""""""""""""""""""


t1=tm.time()

trotter_num=100
Nup=len(up_atm)
Ndn=len(dn_atm)

print(Nup, Ndn)

T, NEUP, NEDN=ED.tevol_new_exp(L,Nup,Ndn, J, Delta_random, Delta_aubry_andre, phi_aubry_andre, Delta_up, Delta_dn, alpha, up_atm, dn_atm, U, 2*np.pi*ti, 2*np.pi*tf, int(n*(tf-ti)), trotter_num/n)
NOUP=Nup-NEUP
NODN=Ndn-NEDN
Imb_up=(NEUP-NOUP)/Nup

Imb_dn=(NEDN-NODN)/Ndn

Imb=Imb_up*Imb_up[0]
Imb_dn=Imb_dn*Imb_dn[0]

print( tm.time()-t1)

###########################################################################################################

"""""""""""""""""""""""""""""""""""""""""""""""
SECTION  4: SAVING DATA

"""""""""""""""""""""""""""""""""""""""""""""


filename='Imb_ED_L21.pic'
fit_file=open(filename, 'wb')
pickle.dump(T*2*np.pi, fit_file)
pickle.dump(Imb_up, fit_file)
pickle.dump(Imb_dn, fit_file)
fit_file.close()




"""""""""""""""""""""""""""""""""""""""""""""""
SECTION  5: PLOTTING IMBALANCE

"""""""""""""""""""""""""""""""""""""""""""""


plt.rcdefaults()

plt.figure(1)
plt.title('Delta 3, U5, L21')
plt.plot(T*np.pi*2*J, -Imb_up)
plt.xlabel(r'Time $t$ ($\tau$)')
plt.ylabel('imbalance')

plt.show()



