<h1>Introduction</h1>
This code is based on the method described in the supplementary information of [this paper](https://arxiv.org/abs/2010.12965). It can be used to compute the time-evolution of a given initial state of spinful fermions in a 1D lattice under the Fermi-Hubbard Hamiltonian.  The script 'Exact_Diagonalization.py' consists of the actual time evolution function. The script 'run_this_script.py' consists of all the parameters of the Hamiltonian and the initial state. Typically, one needs to edit only the latter script. The parameters are divided into two sections.


<h1>Section 1: Hamiltonian</h1>
We consider spinful Fermions on a lattice with L sites. The Hamiltonian we consider is 

<a href="https://www.codecogs.com/eqnedit.php?latex=H=&space;J\sum_{i=1,&space;\sigma=&space;\downarrow,&space;\uparrow}^L&space;\hat{c}_{i&plus;1,&space;\sigma}^{\dagger}&space;\hat{c}_{i,&space;\sigma}&space;&plus;&space;h.c&space;&plus;&space;\sum_{i=1,&space;\sigma=&space;\downarrow,&space;\uparrow}^L&space;V_{i,&space;\sigma}&space;\hat{c}_{i,&space;\sigma}^{\dagger}&space;\hat{c}_{i,&space;\sigma}&space;&plus;&space;U&space;\sum_{i=1}^L&space;\hat{c}_{i,&space;\uparrow}^{\dagger}&space;\hat{c}_{i,&space;\uparrow}\hat{c}_{i,&space;\downarrow}^{\dagger}&space;\hat{c}_{i,&space;\downarrow}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?H=&space;J\sum_{i=1,&space;\sigma=&space;\downarrow,&space;\uparrow}^L&space;\hat{c}_{i&plus;1,&space;\sigma}^{\dagger}&space;\hat{c}_{i,&space;\sigma}&space;&plus;&space;h.c&space;&plus;&space;\sum_{i=1,&space;\sigma=&space;\downarrow,&space;\uparrow}^L&space;V_{i,&space;\sigma}&space;\hat{c}_{i,&space;\sigma}^{\dagger}&space;\hat{c}_{i,&space;\sigma}&space;&plus;&space;U&space;\sum_{i=1}^L&space;\hat{c}_{i,&space;\uparrow}^{\dagger}&space;\hat{c}_{i,&space;\uparrow}\hat{c}_{i,&space;\downarrow}^{\dagger}&space;\hat{c}_{i,&space;\downarrow}" title="H= J\sum_{i=1, \sigma= \downarrow, \uparrow}^L \hat{c}_{i+1, \sigma}^{\dagger} \hat{c}_{i, \sigma} + h.c + \sum_{i=1, \sigma= \downarrow, \uparrow}^L V_{i, \sigma} \hat{c}_{i, \sigma}^{\dagger} \hat{c}_{i, \sigma} + U \sum_{i=1}^L \hat{c}_{i, \uparrow}^{\dagger} \hat{c}_{i, \uparrow}\hat{c}_{i, \downarrow}^{\dagger} \hat{c}_{i, \downarrow}" /></a>

Although the general code Exact_Diagonalization.py' is designed for any onsite potential, in the script 'run_this_script.py' we use an on-site potential of the form <a href="https://www.codecogs.com/eqnedit.php?latex=V_{i,&space;\sigma}=&space;\Delta_{\sigma}(i-L/2)&space;&plus;&space;\alpha&space;(i-L/2)^2&space;&plus;&space;\delta_{i}&space;&plus;&space;\Delta_{aa}\cos(2\pi&space;\beta&space;i&space;&plus;&space;\phi_{aa}&space;)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?V_{i,&space;\sigma}=&space;\Delta_{\sigma}(i-L/2)&space;&plus;&space;\alpha&space;(i-L/2)^2&space;&plus;&space;\delta_{i}&space;&plus;&space;\Delta_{aa}\cos(2\pi&space;\beta&space;i&space;&plus;&space;\phi_{aa}&space;)" title="V_{i, \sigma}= \Delta_{\sigma}(i-L/2) + \alpha (i-L/2)^2 + \delta_{i} + \Delta_{aa}\cos(2\pi \beta i + \phi_{aa} )" /></a>. Here, <a href="https://www.codecogs.com/eqnedit.php?latex=\delta_i&space;\in&space;[-\Delta_r,&space;\Delta_r]" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\delta_i&space;\in&space;[-\Delta_r,&space;\Delta_r]" title="\delta_i \in [-\Delta_r, \Delta_r]" /></a> is a random onsite potential.

Below is a decription of the variables that appear in secion 1 of the script 'run_this_script.py'

1. J is the hopping rate (as it appears in the above Hamiltonian)
2. Delta_random = <a href="https://www.codecogs.com/eqnedit.php?latex=\Delta_{r}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\Delta_{r}" title="\Delta_{r}" /></a>
3. Delta_aubry_andre = <a href="https://www.codecogs.com/eqnedit.php?latex=\Delta_{aa}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\Delta_{aa}" title="\Delta_{aa}" /></a>
4. phi_aubry_andre= <a href="https://www.codecogs.com/eqnedit.php?latex=\phi_{aa}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\phi_{aa}" title="\phi_{aa}" /></a>
5. Delta_dn = <a href="https://www.codecogs.com/eqnedit.php?latex=\Delta_{\downarrow}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\Delta_{\downarrow}" title="\Delta_{\downarrow}" /></a>
6. Delta_up = <a href="https://www.codecogs.com/eqnedit.php?latex=\Delta_{\downarrow}" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\Delta_{\uparrow}" title="\Delta_{\uparrow}" /></a>
7. U is as in the Hamiltonian.
8. ti is the initial time in milliseconds.
9. tf is the final time in milliseconds
10. n is the number of samples desired per millisecond. Total number of samples is m= int(n*(tf-ti)) +1. 
11. alpha = <a href="https://www.codecogs.com/eqnedit.php?latex=\alpha" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\alpha" title="\alpha" /></a> in the Hamiltonian above. 



<h1>Section 2: System size and initial state</h1>
The varaibles that appear in this section are described below.

1.  L = number of lattice sites. The sites are numbered as 0, 1, 2, ... , L-1.
2.  up_atm = an ordered array of integers less than L indicating the sites occupied by a spin up atom in the initial state. 
3.  dn_atm = an ordered array of integers less than L indicating the sites occupied by a spin down atom in the initial state. 

<h1>Section 3: Time evolution</h1>
The time evolution produces three arrays.

1. T = a 1D array of size m+1 consisting of points in time between ti and tf. 
2. NEUP= a 1D array of size m+1 consisting of the total spin-up occupancy of even sites at each of the m+1 points in time. 
3. NOUP= a 1D array of size m+1 consisting of the total spin-down occupancy of even sites at each of the m+1 points in time. 

The spin resolved even-odd imbalance can be computed as shown in the script. 

<h1>Example</h1>

We consider L=21 sites and an initial state with 

up_atm=[1, 5, 9, 13, 17] and 

dn_atm=[3, 7, 11, 15, 19] 

We compute its time evolution under the stark Hamiltonian with <a href="https://www.codecogs.com/eqnedit.php?latex=\Delta&space;=&space;3J" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\Delta&space;=&space;3J" title="\Delta = 3J" /></a> and U=5J. We show the even-odd imbalance of the spin up atoms from this computation below:

![Figure_1](Figure_1.png)
